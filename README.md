# GGn mUploady

DLsite uploady for GGn.

## Instalation

The script is tested in the latest stable chromium with [violentmonkey](https://violentmonkey.github.io/).

To install, load `./dist/ggn-muploady.user.js` [(click)](https://gitlab.com/mujau/ggn-muploady/-/raw/master/dist/ggn-muploady.user.js) in your userscript manager.

## Building

The project is managed with [pnpm](https://pnpm.io/).

- Run `pnpm i` to install dependencies.
- Run `pnpm build` to build.
