// ==UserScript==
// @name        GGn mUploady
// @namespace   green-is-my-paddy
// @version     0.15.0
// @description DLsite uploady for GGn.
// @author      mujau
// @homepageURL https://gitlab.com/mujau/ggn-muploady
// @downloadURL https://gitlab.com/mujau/ggn-muploady/-/raw/master/dist/ggn-muploady.user.js
// @match       https://gazellegames.net/upload.php
// @require     https://cdn.jsdelivr.net/npm/dompurify@3.1.0/dist/purify.min.js
// @grant       GM_xmlhttpRequest
// @grant       GM.xmlHttpRequest
// @run-at      document-end
// ==/UserScript==

(function (DOMPurify) {
'use strict';

/** @returns {void} */
function noop() {}

const identity = (x) => x;

function run(fn) {
	return fn();
}

function blank_object() {
	return Object.create(null);
}

/**
 * @param {Function[]} fns
 * @returns {void}
 */
function run_all(fns) {
	fns.forEach(run);
}

/**
 * @param {any} thing
 * @returns {thing is Function}
 */
function is_function(thing) {
	return typeof thing === 'function';
}

/** @returns {boolean} */
function safe_not_equal(a, b) {
	return a != a ? b == b : a !== b || (a && typeof a === 'object') || typeof a === 'function';
}

/** @returns {boolean} */
function is_empty(obj) {
	return Object.keys(obj).length === 0;
}

const is_client = typeof window !== 'undefined';

/** @type {() => number} */
let now = is_client ? () => window.performance.now() : () => Date.now();

let raf = is_client ? (cb) => requestAnimationFrame(cb) : noop;

const tasks = new Set();

/**
 * @param {number} now
 * @returns {void}
 */
function run_tasks(now) {
	tasks.forEach((task) => {
		if (!task.c(now)) {
			tasks.delete(task);
			task.f();
		}
	});
	if (tasks.size !== 0) raf(run_tasks);
}

/**
 * Creates a new task that runs on each raf frame
 * until it returns a falsy value or is aborted
 * @param {import('./private.js').TaskCallback} callback
 * @returns {import('./private.js').Task}
 */
function loop(callback) {
	/** @type {import('./private.js').TaskEntry} */
	let task;
	if (tasks.size === 0) raf(run_tasks);
	return {
		promise: new Promise((fulfill) => {
			tasks.add((task = { c: callback, f: fulfill }));
		}),
		abort() {
			tasks.delete(task);
		}
	};
}

/**
 * @param {Node} target
 * @param {Node} node
 * @returns {void}
 */
function append(target, node) {
	target.appendChild(node);
}

/**
 * @param {Node} target
 * @param {string} style_sheet_id
 * @param {string} styles
 * @returns {void}
 */
function append_styles(target, style_sheet_id, styles) {
	const append_styles_to = get_root_for_style(target);
	if (!append_styles_to.getElementById(style_sheet_id)) {
		const style = element('style');
		style.id = style_sheet_id;
		style.textContent = styles;
		append_stylesheet(append_styles_to, style);
	}
}

/**
 * @param {Node} node
 * @returns {ShadowRoot | Document}
 */
function get_root_for_style(node) {
	if (!node) return document;
	const root = node.getRootNode ? node.getRootNode() : node.ownerDocument;
	if (root && /** @type {ShadowRoot} */ (root).host) {
		return /** @type {ShadowRoot} */ (root);
	}
	return node.ownerDocument;
}

/**
 * @param {Node} node
 * @returns {CSSStyleSheet}
 */
function append_empty_stylesheet(node) {
	const style_element = element('style');
	// For transitions to work without 'style-src: unsafe-inline' Content Security Policy,
	// these empty tags need to be allowed with a hash as a workaround until we move to the Web Animations API.
	// Using the hash for the empty string (for an empty tag) works in all browsers except Safari.
	// So as a workaround for the workaround, when we append empty style tags we set their content to /* empty */.
	// The hash 'sha256-9OlNO0DNEeaVzHL4RZwCLsBHA8WBQ8toBp/4F5XV2nc=' will then work even in Safari.
	style_element.textContent = '/* empty */';
	append_stylesheet(get_root_for_style(node), style_element);
	return style_element.sheet;
}

/**
 * @param {ShadowRoot | Document} node
 * @param {HTMLStyleElement} style
 * @returns {CSSStyleSheet}
 */
function append_stylesheet(node, style) {
	append(/** @type {Document} */ (node).head || node, style);
	return style.sheet;
}

/**
 * @param {Node} target
 * @param {Node} node
 * @param {Node} [anchor]
 * @returns {void}
 */
function insert(target, node, anchor) {
	target.insertBefore(node, anchor || null);
}

/**
 * @param {Node} node
 * @returns {void}
 */
function detach(node) {
	if (node.parentNode) {
		node.parentNode.removeChild(node);
	}
}

/**
 * @returns {void} */
function destroy_each(iterations, detaching) {
	for (let i = 0; i < iterations.length; i += 1) {
		if (iterations[i]) iterations[i].d(detaching);
	}
}

/**
 * @template {keyof HTMLElementTagNameMap} K
 * @param {K} name
 * @returns {HTMLElementTagNameMap[K]}
 */
function element(name) {
	return document.createElement(name);
}

/**
 * @param {string} data
 * @returns {Text}
 */
function text(data) {
	return document.createTextNode(data);
}

/**
 * @returns {Text} */
function space() {
	return text(' ');
}

/**
 * @returns {Text} */
function empty() {
	return text('');
}

/**
 * @param {EventTarget} node
 * @param {string} event
 * @param {EventListenerOrEventListenerObject} handler
 * @param {boolean | AddEventListenerOptions | EventListenerOptions} [options]
 * @returns {() => void}
 */
function listen(node, event, handler, options) {
	node.addEventListener(event, handler, options);
	return () => node.removeEventListener(event, handler, options);
}

/**
 * @param {Element} node
 * @param {string} attribute
 * @param {string} [value]
 * @returns {void}
 */
function attr(node, attribute, value) {
	if (value == null) node.removeAttribute(attribute);
	else if (node.getAttribute(attribute) !== value) node.setAttribute(attribute, value);
}

/**
 * @param {Element} element
 * @returns {ChildNode[]}
 */
function children(element) {
	return Array.from(element.childNodes);
}

/**
 * @param {Text} text
 * @param {unknown} data
 * @returns {void}
 */
function set_data(text, data) {
	data = '' + data;
	if (text.data === data) return;
	text.data = /** @type {string} */ (data);
}

/**
 * @returns {void} */
function set_input_value(input, value) {
	input.value = value == null ? '' : value;
}

/**
 * @returns {void} */
function select_option(select, value, mounting) {
	for (let i = 0; i < select.options.length; i += 1) {
		const option = select.options[i];
		if (option.__value === value) {
			option.selected = true;
			return;
		}
	}
	if (!mounting || value !== undefined) {
		select.selectedIndex = -1; // no option should be selected
	}
}

function select_value(select) {
	const selected_option = select.querySelector(':checked');
	return selected_option && selected_option.__value;
}

/**
 * @template T
 * @param {string} type
 * @param {T} [detail]
 * @param {{ bubbles?: boolean, cancelable?: boolean }} [options]
 * @returns {CustomEvent<T>}
 */
function custom_event(type, detail, { bubbles = false, cancelable = false } = {}) {
	return new CustomEvent(type, { detail, bubbles, cancelable });
}

function construct_svelte_component(component, props) {
	return new component(props);
}

/**
 * @typedef {Node & {
 * 	claim_order?: number;
 * 	hydrate_init?: true;
 * 	actual_end_child?: NodeEx;
 * 	childNodes: NodeListOf<NodeEx>;
 * }} NodeEx
 */

/** @typedef {ChildNode & NodeEx} ChildNodeEx */

/** @typedef {NodeEx & { claim_order: number }} NodeEx2 */

/**
 * @typedef {ChildNodeEx[] & {
 * 	claim_info?: {
 * 		last_index: number;
 * 		total_claimed: number;
 * 	};
 * }} ChildNodeArray
 */

// we need to store the information for multiple documents because a Svelte application could also contain iframes
// https://github.com/sveltejs/svelte/issues/3624
/** @type {Map<Document | ShadowRoot, import('./private.d.ts').StyleInformation>} */
const managed_styles = new Map();

let active = 0;

// https://github.com/darkskyapp/string-hash/blob/master/index.js
/**
 * @param {string} str
 * @returns {number}
 */
function hash(str) {
	let hash = 5381;
	let i = str.length;
	while (i--) hash = ((hash << 5) - hash) ^ str.charCodeAt(i);
	return hash >>> 0;
}

/**
 * @param {Document | ShadowRoot} doc
 * @param {Element & ElementCSSInlineStyle} node
 * @returns {{ stylesheet: any; rules: {}; }}
 */
function create_style_information(doc, node) {
	const info = { stylesheet: append_empty_stylesheet(node), rules: {} };
	managed_styles.set(doc, info);
	return info;
}

/**
 * @param {Element & ElementCSSInlineStyle} node
 * @param {number} a
 * @param {number} b
 * @param {number} duration
 * @param {number} delay
 * @param {(t: number) => number} ease
 * @param {(t: number, u: number) => string} fn
 * @param {number} uid
 * @returns {string}
 */
function create_rule(node, a, b, duration, delay, ease, fn, uid = 0) {
	const step = 16.666 / duration;
	let keyframes = '{\n';
	for (let p = 0; p <= 1; p += step) {
		const t = a + (b - a) * ease(p);
		keyframes += p * 100 + `%{${fn(t, 1 - t)}}\n`;
	}
	const rule = keyframes + `100% {${fn(b, 1 - b)}}\n}`;
	const name = `__svelte_${hash(rule)}_${uid}`;
	const doc = get_root_for_style(node);
	const { stylesheet, rules } = managed_styles.get(doc) || create_style_information(doc, node);
	if (!rules[name]) {
		rules[name] = true;
		stylesheet.insertRule(`@keyframes ${name} ${rule}`, stylesheet.cssRules.length);
	}
	const animation = node.style.animation || '';
	node.style.animation = `${
		animation ? `${animation}, ` : ''
	}${name} ${duration}ms linear ${delay}ms 1 both`;
	active += 1;
	return name;
}

/**
 * @param {Element & ElementCSSInlineStyle} node
 * @param {string} [name]
 * @returns {void}
 */
function delete_rule(node, name) {
	const previous = (node.style.animation || '').split(', ');
	const next = previous.filter(
		name
			? (anim) => anim.indexOf(name) < 0 // remove specific animation
			: (anim) => anim.indexOf('__svelte') === -1 // remove all Svelte animations
	);
	const deleted = previous.length - next.length;
	if (deleted) {
		node.style.animation = next.join(', ');
		active -= deleted;
		if (!active) clear_rules();
	}
}

/** @returns {void} */
function clear_rules() {
	raf(() => {
		if (active) return;
		managed_styles.forEach((info) => {
			const { ownerNode } = info.stylesheet;
			// there is no ownerNode if it runs on jsdom.
			if (ownerNode) detach(ownerNode);
		});
		managed_styles.clear();
	});
}

let current_component;

/** @returns {void} */
function set_current_component(component) {
	current_component = component;
}

const dirty_components = [];
const binding_callbacks = [];

let render_callbacks = [];

const flush_callbacks = [];

const resolved_promise = /* @__PURE__ */ Promise.resolve();

let update_scheduled = false;

/** @returns {void} */
function schedule_update() {
	if (!update_scheduled) {
		update_scheduled = true;
		resolved_promise.then(flush);
	}
}

/** @returns {void} */
function add_render_callback(fn) {
	render_callbacks.push(fn);
}

// flush() calls callbacks in this order:
// 1. All beforeUpdate callbacks, in order: parents before children
// 2. All bind:this callbacks, in reverse order: children before parents.
// 3. All afterUpdate callbacks, in order: parents before children. EXCEPT
//    for afterUpdates called during the initial onMount, which are called in
//    reverse order: children before parents.
// Since callbacks might update component values, which could trigger another
// call to flush(), the following steps guard against this:
// 1. During beforeUpdate, any updated components will be added to the
//    dirty_components array and will cause a reentrant call to flush(). Because
//    the flush index is kept outside the function, the reentrant call will pick
//    up where the earlier call left off and go through all dirty components. The
//    current_component value is saved and restored so that the reentrant call will
//    not interfere with the "parent" flush() call.
// 2. bind:this callbacks cannot trigger new flush() calls.
// 3. During afterUpdate, any updated components will NOT have their afterUpdate
//    callback called a second time; the seen_callbacks set, outside the flush()
//    function, guarantees this behavior.
const seen_callbacks = new Set();

let flushidx = 0; // Do *not* move this inside the flush() function

/** @returns {void} */
function flush() {
	// Do not reenter flush while dirty components are updated, as this can
	// result in an infinite loop. Instead, let the inner flush handle it.
	// Reentrancy is ok afterwards for bindings etc.
	if (flushidx !== 0) {
		return;
	}
	const saved_component = current_component;
	do {
		// first, call beforeUpdate functions
		// and update components
		try {
			while (flushidx < dirty_components.length) {
				const component = dirty_components[flushidx];
				flushidx++;
				set_current_component(component);
				update(component.$$);
			}
		} catch (e) {
			// reset dirty state to not end up in a deadlocked state and then rethrow
			dirty_components.length = 0;
			flushidx = 0;
			throw e;
		}
		set_current_component(null);
		dirty_components.length = 0;
		flushidx = 0;
		while (binding_callbacks.length) binding_callbacks.pop()();
		// then, once components are updated, call
		// afterUpdate functions. This may cause
		// subsequent updates...
		for (let i = 0; i < render_callbacks.length; i += 1) {
			const callback = render_callbacks[i];
			if (!seen_callbacks.has(callback)) {
				// ...so guard against infinite loops
				seen_callbacks.add(callback);
				callback();
			}
		}
		render_callbacks.length = 0;
	} while (dirty_components.length);
	while (flush_callbacks.length) {
		flush_callbacks.pop()();
	}
	update_scheduled = false;
	seen_callbacks.clear();
	set_current_component(saved_component);
}

/** @returns {void} */
function update($$) {
	if ($$.fragment !== null) {
		$$.update();
		run_all($$.before_update);
		const dirty = $$.dirty;
		$$.dirty = [-1];
		$$.fragment && $$.fragment.p($$.ctx, dirty);
		$$.after_update.forEach(add_render_callback);
	}
}

/**
 * Useful for example to execute remaining `afterUpdate` callbacks before executing `destroy`.
 * @param {Function[]} fns
 * @returns {void}
 */
function flush_render_callbacks(fns) {
	const filtered = [];
	const targets = [];
	render_callbacks.forEach((c) => (fns.indexOf(c) === -1 ? filtered.push(c) : targets.push(c)));
	targets.forEach((c) => c());
	render_callbacks = filtered;
}

/**
 * @type {Promise<void> | null}
 */
let promise;

/**
 * @returns {Promise<void>}
 */
function wait() {
	if (!promise) {
		promise = Promise.resolve();
		promise.then(() => {
			promise = null;
		});
	}
	return promise;
}

/**
 * @param {Element} node
 * @param {INTRO | OUTRO | boolean} direction
 * @param {'start' | 'end'} kind
 * @returns {void}
 */
function dispatch(node, direction, kind) {
	node.dispatchEvent(custom_event(`${direction ? 'intro' : 'outro'}${kind}`));
}

const outroing = new Set();

/**
 * @type {Outro}
 */
let outros;

/**
 * @returns {void} */
function group_outros() {
	outros = {
		r: 0,
		c: [],
		p: outros // parent group
	};
}

/**
 * @returns {void} */
function check_outros() {
	if (!outros.r) {
		run_all(outros.c);
	}
	outros = outros.p;
}

/**
 * @param {import('./private.js').Fragment} block
 * @param {0 | 1} [local]
 * @returns {void}
 */
function transition_in(block, local) {
	if (block && block.i) {
		outroing.delete(block);
		block.i(local);
	}
}

/**
 * @param {import('./private.js').Fragment} block
 * @param {0 | 1} local
 * @param {0 | 1} [detach]
 * @param {() => void} [callback]
 * @returns {void}
 */
function transition_out(block, local, detach, callback) {
	if (block && block.o) {
		if (outroing.has(block)) return;
		outroing.add(block);
		outros.c.push(() => {
			outroing.delete(block);
			if (callback) {
				if (detach) block.d(1);
				callback();
			}
		});
		block.o(local);
	} else if (callback) {
		callback();
	}
}

/**
 * @type {import('../transition/public.js').TransitionConfig}
 */
const null_transition = { duration: 0 };

/**
 * @param {Element & ElementCSSInlineStyle} node
 * @param {TransitionFn} fn
 * @param {any} params
 * @param {boolean} intro
 * @returns {{ run(b: 0 | 1): void; end(): void; }}
 */
function create_bidirectional_transition(node, fn, params, intro) {
	/**
	 * @type {TransitionOptions} */
	const options = { direction: 'both' };
	let config = fn(node, params, options);
	let t = intro ? 0 : 1;

	/**
	 * @type {Program | null} */
	let running_program = null;

	/**
	 * @type {PendingProgram | null} */
	let pending_program = null;
	let animation_name = null;

	/** @type {boolean} */
	let original_inert_value;

	/**
	 * @returns {void} */
	function clear_animation() {
		if (animation_name) delete_rule(node, animation_name);
	}

	/**
	 * @param {PendingProgram} program
	 * @param {number} duration
	 * @returns {Program}
	 */
	function init(program, duration) {
		const d = /** @type {Program['d']} */ (program.b - t);
		duration *= Math.abs(d);
		return {
			a: t,
			b: program.b,
			d,
			duration,
			start: program.start,
			end: program.start + duration,
			group: program.group
		};
	}

	/**
	 * @param {INTRO | OUTRO} b
	 * @returns {void}
	 */
	function go(b) {
		const {
			delay = 0,
			duration = 300,
			easing = identity,
			tick = noop,
			css
		} = config || null_transition;

		/**
		 * @type {PendingProgram} */
		const program = {
			start: now() + delay,
			b
		};

		if (!b) {
			// @ts-ignore todo: improve typings
			program.group = outros;
			outros.r += 1;
		}

		if ('inert' in node) {
			if (b) {
				if (original_inert_value !== undefined) {
					// aborted/reversed outro — restore previous inert value
					node.inert = original_inert_value;
				}
			} else {
				original_inert_value = /** @type {HTMLElement} */ (node).inert;
				node.inert = true;
			}
		}

		if (running_program || pending_program) {
			pending_program = program;
		} else {
			// if this is an intro, and there's a delay, we need to do
			// an initial tick and/or apply CSS animation immediately
			if (css) {
				clear_animation();
				animation_name = create_rule(node, t, b, duration, delay, easing, css);
			}
			if (b) tick(0, 1);
			running_program = init(program, duration);
			add_render_callback(() => dispatch(node, b, 'start'));
			loop((now) => {
				if (pending_program && now > pending_program.start) {
					running_program = init(pending_program, duration);
					pending_program = null;
					dispatch(node, running_program.b, 'start');
					if (css) {
						clear_animation();
						animation_name = create_rule(
							node,
							t,
							running_program.b,
							running_program.duration,
							0,
							easing,
							config.css
						);
					}
				}
				if (running_program) {
					if (now >= running_program.end) {
						tick((t = running_program.b), 1 - t);
						dispatch(node, running_program.b, 'end');
						if (!pending_program) {
							// we're done
							if (running_program.b) {
								// intro — we can tidy up immediately
								clear_animation();
							} else {
								// outro — needs to be coordinated
								if (!--running_program.group.r) run_all(running_program.group.c);
							}
						}
						running_program = null;
					} else if (now >= running_program.start) {
						const p = now - running_program.start;
						t = running_program.a + running_program.d * easing(p / running_program.duration);
						tick(t, 1 - t);
					}
				}
				return !!(running_program || pending_program);
			});
		}
	}
	return {
		run(b) {
			if (is_function(config)) {
				wait().then(() => {
					const opts = { direction: b ? 'in' : 'out' };
					// @ts-ignore
					config = config(opts);
					go(b);
				});
			} else {
				go(b);
			}
		},
		end() {
			clear_animation();
			running_program = pending_program = null;
		}
	};
}

/** @typedef {1} INTRO */
/** @typedef {0} OUTRO */
/** @typedef {{ direction: 'in' | 'out' | 'both' }} TransitionOptions */
/** @typedef {(node: Element, params: any, options: TransitionOptions) => import('../transition/public.js').TransitionConfig} TransitionFn */

/**
 * @typedef {Object} Outro
 * @property {number} r
 * @property {Function[]} c
 * @property {Object} p
 */

/**
 * @typedef {Object} PendingProgram
 * @property {number} start
 * @property {INTRO|OUTRO} b
 * @property {Outro} [group]
 */

/**
 * @typedef {Object} Program
 * @property {number} a
 * @property {INTRO|OUTRO} b
 * @property {1|-1} d
 * @property {number} duration
 * @property {number} start
 * @property {number} end
 * @property {Outro} [group]
 */

// general each functions:

function ensure_array_like(array_like_or_iterator) {
	return array_like_or_iterator?.length !== undefined
		? array_like_or_iterator
		: Array.from(array_like_or_iterator);
}

/** @returns {void} */
function create_component(block) {
	block && block.c();
}

/** @returns {void} */
function mount_component(component, target, anchor) {
	const { fragment, after_update } = component.$$;
	fragment && fragment.m(target, anchor);
	// onMount happens before the initial afterUpdate
	add_render_callback(() => {
		const new_on_destroy = component.$$.on_mount.map(run).filter(is_function);
		// if the component was destroyed immediately
		// it will update the `$$.on_destroy` reference to `null`.
		// the destructured on_destroy may still reference to the old array
		if (component.$$.on_destroy) {
			component.$$.on_destroy.push(...new_on_destroy);
		} else {
			// Edge case - component was destroyed immediately,
			// most likely as a result of a binding initialising
			run_all(new_on_destroy);
		}
		component.$$.on_mount = [];
	});
	after_update.forEach(add_render_callback);
}

/** @returns {void} */
function destroy_component(component, detaching) {
	const $$ = component.$$;
	if ($$.fragment !== null) {
		flush_render_callbacks($$.after_update);
		run_all($$.on_destroy);
		$$.fragment && $$.fragment.d(detaching);
		// TODO null out other refs, including component.$$ (but need to
		// preserve final state?)
		$$.on_destroy = $$.fragment = null;
		$$.ctx = [];
	}
}

/** @returns {void} */
function make_dirty(component, i) {
	if (component.$$.dirty[0] === -1) {
		dirty_components.push(component);
		schedule_update();
		component.$$.dirty.fill(0);
	}
	component.$$.dirty[(i / 31) | 0] |= 1 << i % 31;
}

// TODO: Document the other params
/**
 * @param {SvelteComponent} component
 * @param {import('./public.js').ComponentConstructorOptions} options
 *
 * @param {import('./utils.js')['not_equal']} not_equal Used to compare props and state values.
 * @param {(target: Element | ShadowRoot) => void} [append_styles] Function that appends styles to the DOM when the component is first initialised.
 * This will be the `add_css` function from the compiled component.
 *
 * @returns {void}
 */
function init(
	component,
	options,
	instance,
	create_fragment,
	not_equal,
	props,
	append_styles = null,
	dirty = [-1]
) {
	const parent_component = current_component;
	set_current_component(component);
	/** @type {import('./private.js').T$$} */
	const $$ = (component.$$ = {
		fragment: null,
		ctx: [],
		// state
		props,
		update: noop,
		not_equal,
		bound: blank_object(),
		// lifecycle
		on_mount: [],
		on_destroy: [],
		on_disconnect: [],
		before_update: [],
		after_update: [],
		context: new Map(options.context || (parent_component ? parent_component.$$.context : [])),
		// everything else
		callbacks: blank_object(),
		dirty,
		skip_bound: false,
		root: options.target || parent_component.$$.root
	});
	append_styles && append_styles($$.root);
	let ready = false;
	$$.ctx = instance
		? instance(component, options.props || {}, (i, ret, ...rest) => {
				const value = rest.length ? rest[0] : ret;
				if ($$.ctx && not_equal($$.ctx[i], ($$.ctx[i] = value))) {
					if (!$$.skip_bound && $$.bound[i]) $$.bound[i](value);
					if (ready) make_dirty(component, i);
				}
				return ret;
		  })
		: [];
	$$.update();
	ready = true;
	run_all($$.before_update);
	// `false` as a special case of no DOM component
	$$.fragment = create_fragment ? create_fragment($$.ctx) : false;
	if (options.target) {
		if (options.hydrate) {
			// TODO: what is the correct type here?
			// @ts-expect-error
			const nodes = children(options.target);
			$$.fragment && $$.fragment.l(nodes);
			nodes.forEach(detach);
		} else {
			// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
			$$.fragment && $$.fragment.c();
		}
		if (options.intro) transition_in(component.$$.fragment);
		mount_component(component, options.target, options.anchor);
		flush();
	}
	set_current_component(parent_component);
}

/**
 * Base class for Svelte components. Used when dev=false.
 *
 * @template {Record<string, any>} [Props=any]
 * @template {Record<string, any>} [Events=any]
 */
class SvelteComponent {
	/**
	 * ### PRIVATE API
	 *
	 * Do not use, may change at any time
	 *
	 * @type {any}
	 */
	$$ = undefined;
	/**
	 * ### PRIVATE API
	 *
	 * Do not use, may change at any time
	 *
	 * @type {any}
	 */
	$$set = undefined;

	/** @returns {void} */
	$destroy() {
		destroy_component(this, 1);
		this.$destroy = noop;
	}

	/**
	 * @template {Extract<keyof Events, string>} K
	 * @param {K} type
	 * @param {((e: Events[K]) => void) | null | undefined} callback
	 * @returns {() => void}
	 */
	$on(type, callback) {
		if (!is_function(callback)) {
			return noop;
		}
		const callbacks = this.$$.callbacks[type] || (this.$$.callbacks[type] = []);
		callbacks.push(callback);
		return () => {
			const index = callbacks.indexOf(callback);
			if (index !== -1) callbacks.splice(index, 1);
		};
	}

	/**
	 * @param {Partial<Props>} props
	 * @returns {void}
	 */
	$set(props) {
		if (this.$$set && !is_empty(props)) {
			this.$$.skip_bound = true;
			this.$$set(props);
			this.$$.skip_bound = false;
		}
	}
}

/**
 * @typedef {Object} CustomElementPropDefinition
 * @property {string} [attribute]
 * @property {boolean} [reflect]
 * @property {'String'|'Boolean'|'Number'|'Array'|'Object'} [type]
 */

// generated during release, do not modify

const PUBLIC_VERSION = '4';

if (typeof window !== 'undefined')
	// @ts-ignore
	(window.__svelte || (window.__svelte = { v: new Set() })).v.add(PUBLIC_VERSION);

/*
Adapted from https://github.com/mattdesl
Distributed under MIT License https://github.com/mattdesl/eases/blob/master/LICENSE.md
*/

/**
 * https://svelte.dev/docs/svelte-easing
 * @param {number} t
 * @returns {number}
 */
function cubicOut(t) {
	const f = t - 1.0;
	return f * f * f + 1.0;
}

/**
 * Slides an element in and out.
 *
 * https://svelte.dev/docs/svelte-transition#slide
 * @param {Element} node
 * @param {import('./public').SlideParams} [params]
 * @returns {import('./public').TransitionConfig}
 */
function slide(node, { delay = 0, duration = 400, easing = cubicOut, axis = 'y' } = {}) {
	const style = getComputedStyle(node);
	const opacity = +style.opacity;
	const primary_property = axis === 'y' ? 'height' : 'width';
	const primary_property_value = parseFloat(style[primary_property]);
	const secondary_properties = axis === 'y' ? ['top', 'bottom'] : ['left', 'right'];
	const capitalized_secondary_properties = secondary_properties.map(
		(e) => `${e[0].toUpperCase()}${e.slice(1)}`
	);
	const padding_start_value = parseFloat(style[`padding${capitalized_secondary_properties[0]}`]);
	const padding_end_value = parseFloat(style[`padding${capitalized_secondary_properties[1]}`]);
	const margin_start_value = parseFloat(style[`margin${capitalized_secondary_properties[0]}`]);
	const margin_end_value = parseFloat(style[`margin${capitalized_secondary_properties[1]}`]);
	const border_width_start_value = parseFloat(
		style[`border${capitalized_secondary_properties[0]}Width`]
	);
	const border_width_end_value = parseFloat(
		style[`border${capitalized_secondary_properties[1]}Width`]
	);
	return {
		delay,
		duration,
		easing,
		css: (t) =>
			'overflow: hidden;' +
			`opacity: ${Math.min(t * 20, 1) * opacity};` +
			`${primary_property}: ${t * primary_property_value}px;` +
			`padding-${secondary_properties[0]}: ${t * padding_start_value}px;` +
			`padding-${secondary_properties[1]}: ${t * padding_end_value}px;` +
			`margin-${secondary_properties[0]}: ${t * margin_start_value}px;` +
			`margin-${secondary_properties[1]}: ${t * margin_end_value}px;` +
			`border-${secondary_properties[0]}-width: ${t * border_width_start_value}px;` +
			`border-${secondary_properties[1]}-width: ${t * border_width_end_value}px;`
	};
}

const xhr = GM_xmlhttpRequest || GM.xmlHttpRequest;
/**
 * A promise based wrapper for `GM_xmlhttpRequest` similar to native `fetch`.
 * @param details - An object with `details` that should be passed to `GM_xmlhttpRequest`.
 * `onabort`, `onerror`, `ontimeout` and `onload` properties are overriden.
 * @param signal - Abort signal.
 * @returns A promise resolving to `GM_xmlhttpRequest`'s `Response` object.
 */
function promisingXHR(details, signal) {
    return new Promise((resolve, reject) => {
        Object.assign(details, {
            onabort: (response) => {
                reject(response);
            },
            onerror: (response) => {
                reject(response);
            },
            ontimeout: (response) => {
                reject(response);
            },
            onload: (response) => {
                resolve(response);
            },
        });
        const { abort } = xhr(details);
        if (signal && signal instanceof AbortSignal) {
            signal.addEventListener("abort", () => abort(), { once: true });
        }
    });
}

const LocaleMap = {
    "ja": "ja_JP",
    "zh-TW": "zh-TW",
    "zh-CN": "zh-CN",
    "ko": "ko_KR",
    "en": "en_US",
};

const ProductFields = [
    "age_category",
    "age_category_string",
    "maker_name",
    "machine",
    "cpu",
    "directx",
    "etc",
    "hdd",
    "memory",
    "vram",
    "regist_date",
    "site_id",
    "workno",
    "image_main",
    "image_samples",
    "work_name",
    "work_name_kana",
    "work_type",
    "work_type_string",
];
/**
 * Queries DLsite's API for product details.
 * @param code - Product ID.
 * @param locale - Response's language.
 * @param signal - Abort signal.
 * @returns Product promise.
 */
async function getProduct(code, locale, signal) {
    const URL = "https://www.dlsite.com/home/api/=/product.json";
    const headers = { "X-Requested-With": "XMLHttpRequest" };
    const searchParams = new URLSearchParams({
        workno: code,
        is_ana: "0",
        fields: ProductFields.join(","),
    });
    if (locale) {
        searchParams.append("locale", LocaleMap[locale]);
    }
    const fullURL = URL + "?" + searchParams.toString();
    const { status, responseText } = await promisingXHR({
        headers: headers,
        method: "GET",
        url: fullURL,
        anonymous: true,
    }, signal);
    if (status < 200 || status > 299) {
        console.debug("Bad response body: %s", responseText);
        throw new Error(`Bad response status: ${status}`);
    }
    const json = JSON.parse(responseText);
    if (!Array.isArray(json)) {
        console.debug("Not an array: %o", json);
        throw new Error("API response is expected to be an array");
    }
    if (!json.length) {
        console.debug("Empty response from %s", fullURL);
        throw new Error("Empty API response");
    }
    if (json.length !== 1) {
        console.debug("More than one result: %o", json);
        throw new Error("The API returned more than one result");
    }
    const product = json[0];
    return product;
}

/**
 * Performs replacements on descendants of the provided element.
 * @param doc - An object compatible with `querySelectorAll`.
 * @param replacements - An object mapping selectors to replacer functions.
 * @param target - The property that should be replaced. `outerHTML` by default.
 */
function queryReplacer(doc, replacements, target = "outerHTML") {
    for (const [selectors, replacer] of Object.entries(replacements)) {
        const nodes = doc.querySelectorAll(selectors);
        for (const node of nodes) {
            node[target] = replacer(node);
        }
    }
}

const trimWhitespaceMap = new Map([
    [/^ +| +$/gm, ""], // Trim leading and trailing spaces (per line).
    [/ {2,}/g, " "], // Collapse multiple spaces.
    [/\n{3,}/g, "\n\n"], // Collapse more than two linebreaks (one empty line).
    [/(\[\/b\]\n)\n+/g, "$1"], // Collapse linebreaks after [b] tags.
]);
/**
 * Trims various whitespace patterns.
 * @param str - A string for trimming.
 * @returns A trimmed string.
 */
function trimWhitespace(str) {
    for (const [regex, replacement] of trimWhitespaceMap) {
        str = str.replace(regex, replacement);
    }
    str = str.trim();
    str += "\n"; // Terminate the string with a newline.
    return str;
}
/**
 * Patterns for extracting data from list items.
 */
const listItemMatcherMap = {
    "-": /^- *(.+)/gm,
    "*": /^\* *(.+)/gm,
    "digit": /^\d{1,2}\. *(.+)/gm,
};
/**
 * Patterns for finding lists.
 */
const listMatcherMap = {
    "-": /(?:^(-).+\n(?:^(?!\1).+\n)?){2,}/gm, // The optional part allows it to consume a single plain line.
    "*": /(?:^(\*).+\n(?:^(?!\1).+\n)?){2,}/gm,
    "digit": /(?:^(\d{1,2})\..+\n(?:^(?!\1).+\n)?){2,}/gm,
};
/**
 * Transforms all list items to BBcode form.
 * @param list - A string containing all list items.
 * @param bullet - The symbol used as a bullet by the list.
 * @returns A string containing BBcodified list.
 */
function listReplacer(list, bullet) {
    return list.replace(listItemMatcherMap[bullet], "[*]$1");
}
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const DLsite2BBMap = new Map([
    [/^\[([^\]]+)\]$\n{0,3}/gm, "\n[b]$1[/b]\n"], // [foo] header kind.
    [/^\* *(.+?) *\*$\n{0,3}/gm, "\n[b]$1[/b]\n"], // * foo * header kind.
    [listMatcherMap["-"], listReplacer],
    [listMatcherMap["*"], listReplacer],
    [listMatcherMap["digit"], (list) => listReplacer(list, "digit")],
    [/([^\n]+[.,!?;])\n(?!\n|\[\*\])/g, "$1 "], // Single linebreak purging for lines terminated with punctuation. Excludes list items.
]);
/**
 * Trims the string and tries to replace recognizable patterns with BBcode.
 * @param str - A string to be BBcodified.
 * @returns A BBcodified string.
 */
function DLsite2BB(str) {
    str = trimWhitespace(str);
    for (const [regex, replacement] of DLsite2BBMap) {
        str = str.replace(regex, replacement);
    }
    return trimWhitespace(str);
}

const CodeToSiteMap = new Map([
    ["RJ", "maniax"],
    ["RE", "maniax"], // ecchi_eng is deprecated
    ["VJ", "pro"],
    ["BJ", "books"],
]);
/**
 * Gathers info needed for building product url.
 * @param product - Product info object or product id.
 * @returns An object with `site_id` and `workno` properties.
 */
function resolve(product) {
    if (typeof product === "string") {
        const firstTwo = product.slice(0, 2);
        const site_id = CodeToSiteMap.get(firstTwo) ?? "home"; // Home should work anyway.
        return { site_id, workno: product };
    }
    else {
        return product;
    }
}
/**
 * Builds DLsite product url from product data or product id.
 * @remarks Id resolution is less accurate, but can be executed asynchronously.
 * @param product - Product info object or product id.
 * @param locale - Page's language.
 * @returns Product url.
 */
function getGamePageUrl(product, locale) {
    const { site_id, workno } = resolve(product);
    const query = locale ? `/?locale=${LocaleMap[locale]}` : "";
    return `https://www.dlsite.com/${site_id}/work/=/product_id/${workno}.html${query}`;
}

/**
 * Fetches game's page and returns it as a sanitized DocumentFragment.
 * @param product - Product id or product info object.
 * @param locale - Page's language.
 * @param signal - Abort signal.
 * @returns A clean DocumentFragment.
 */
async function getGamePage(product, locale, signal) {
    const url = getGamePageUrl(product, locale);
    const { responseText, status, finalUrl } = await promisingXHR({
        method: "GET",
        url: url,
        anonymous: true,
    }, signal);
    if (status === 404) {
        throw new Error("No related products");
    }
    if (status < 200 || status > 299) {
        throw new Error(`Bad response status: ${status}`);
    }
    if (url !== finalUrl) {
        throw new Error("Redirect");
    }
    const clean = DOMPurify.sanitize(responseText, {
        FORBID_TAGS: ["img"],
        KEEP_CONTENT: true,
        RETURN_DOM_FRAGMENT: true,
    });
    return clean;
}

const htmlToBBcodeMap = {
    "p": ({ innerHTML }) => `\n\n${innerHTML}\n\n`,
    "h1, h2, h3, h4, h5": ({ innerHTML }) => `\n[b]${innerHTML}[/b]\n`,
    "br": () => "\n",
    "a": () => "",
};
/**
 * Clean ups game's description and converts it to BBcode.
 * @param desc - A string containing game's description.
 * @returns A string containing game's cleaned up description converted to BBcode.
 */
function cleanDescription(desc) {
    /** Nuke existing line breaks to make place for the ones parsed from <br> tags. */
    desc = desc.replace(/\n/g, "");
    const parser = new DOMParser();
    const descDoc = parser.parseFromString(desc, "text/html");
    queryReplacer(descDoc, htmlToBBcodeMap, "outerHTML"); // HTML tags to BBcode.
    return DLsite2BB(descDoc.body.textContent ?? ""); // Other patterns and cleanup.
}
/**
 * Extracts game's description from the DOM.
 * @param doc - Game's page.
 * @returns A string containing game's description.
 */
function extractDescription(doc) {
    const parts = doc.querySelectorAll(".work_parts");
    let desc = "";
    for (let i = 0; i < parts.length; i++) {
        desc += parts[i].innerHTML;
    }
    return desc;
}
/**
 * Fetches game's page and returns its description.
 * @param product - Product id or product info object.
 * @param locale - Page's language.
 * @param signal - Abort signal.
 * @returns A promise of game's description.
 */
async function getDescription(product, locale, signal) {
    const doc = await getGamePage(product, locale, signal);
    const desc = extractDescription(doc);
    return cleanDescription(desc);
}

const windowsMap = new Map([
    ["W95", "95"],
    ["W98", "98"],
    ["WME", "Me"],
    ["W20", "2000"],
    ["WXP", "XP"],
    ["VIS", "Vista"],
    ["W07", "7"],
    ["W08", "8"],
    ["W81", "8.1"],
    ["W10", "10"],
    ["W11", "11"], // future proofing
]);
const Pattern = {
    MacOS: /^MC.$/,
    Android: /^AND(\d{1,3}(?:\.\d{1,3}){0,2})\+$/,
    iOS: /^IOS(\d{1,3}(?:\.\d{1,3}){0,2})\+$/,
};
/**
 * Parses OS compatibility string to an object.
 * @param machine - A string listing systems the product is compatible with.
 * @returns An object with OS compatibility info.
 */
function parseMachineString(machine) {
    const OS = {
        Windows: [],
        MacOS: false,
        Android: "",
        iOS: "",
        Other: [],
    };
    for (const system of machine.split("#")) {
        if (!system) {
            continue;
        }
        if (windowsMap.has(system)) {
            OS.Windows.push(windowsMap.get(system));
        }
        else if (Pattern.MacOS.test(system)) {
            OS.MacOS = true;
        }
        else {
            const found = ["Android", "iOS"].find((mobile) => {
                const match = Pattern[mobile].exec(system);
                if (match) {
                    OS[mobile] = `${mobile} ${match[1]}+`;
                    return true;
                }
            });
            if (!found) {
                OS.Other.push(system);
            }
        }
    }
    return OS;
}
/**
 * Converts DLsite's OS compatibility list to something less ugly.
 * @param machine - A string listing systems the product is compatible with.
 * @returns A human friendly OS compatibility list.
 */
function formatMachineString(machine) {
    if (!machine) {
        return null;
    }
    const OS = parseMachineString(machine);
    const parts = [];
    if (OS.Windows.length > 0) {
        parts.push("Windows " + OS.Windows.join(" / "));
    }
    if (OS.MacOS) {
        parts.push("MacOS");
    }
    if (OS.Android) {
        parts.push(OS.Android);
    }
    if (OS.iOS) {
        parts.push(OS.iOS);
    }
    if (OS.Other.length > 0) {
        parts.push(OS.Other.join(" | "));
    }
    return parts.join(" | ");
}

const DLsiteCodeRegex = /^(RJ|RE|VJ|BJ)(\d{4}|\d{6}|\d{8})$/i;
/**
 * Throws an error if the code is not in the proper format.
 * @param code - A string to assert as DLsite product ID.
 */
function assertValidCode(code) {
    if (!DLsiteCodeRegex.test(code)) {
        console.debug("Bad code: %s", code);
        throw new Error("Bad code format");
    }
}

const DLsite = {
    getProduct,
    getDescription,
    getGamePageUrl,
    formatMachineString,
};

const isInputElement = (elem) => elem instanceof HTMLInputElement;
const isTextAreaElement = (elem) => elem instanceof HTMLTextAreaElement;
const isSelectElement = (elem) => elem instanceof HTMLSelectElement;
const hasValue = (elem) => isInputElement(elem) || isTextAreaElement(elem) || isSelectElement(elem);
const getElementValue = (elem) => (hasValue(elem) ? elem.value : "");
const setElementValue = (elem, value) => hasValue(elem) && !elem.disabled ? (elem.value = String(value)) : undefined;
/**
 * Queries DOM with provided selector string and asserts the returned element is of correct type.
 * @param selector - A string with selectors.
 * @param Constructor - A constructor extending HTMLElement.
 * @returns A narrowed-down HTMLElement or undefined.
 */
function getElement(selector, Constructor) {
    const elem = document.querySelector(selector);
    return elem instanceof Constructor ? elem : undefined;
}
/**
 * Binds DOM elements to properties for easy access.
 * @privateRemarks Overengineered.
 * @param elements - An object describing DOM elements that should be binded.
 * @returns An object with binded DOM elements.
 */
function getDOMAccessor(elements) {
    const accessor = {};
    for (const [name, [selector, Constructor]] of Object.entries(elements)) {
        const element = getElement(selector, Constructor);
        if (!element) {
            throw new ReferenceError(`${name} not found`);
        }
        Object.defineProperty(accessor, `${name}Element`, {
            enumerable: true,
            get: () => element,
        });
        if (hasValue(element)) {
            Object.defineProperty(accessor, name, {
                enumerable: true,
                get: () => getElementValue(element),
                set: (value) => setElementValue(element, value),
            });
        }
    }
    return accessor;
}

const elements = {
    titleRow: ["#title_tr", HTMLTableRowElement],
    title: ["#title", HTMLInputElement],
    platform: ["#platform", HTMLSelectElement],
    aliases: ["#aliases", HTMLInputElement],
    tags: ["#tags", HTMLInputElement],
    year: ["#year", HTMLInputElement],
    rating: ["#Rating", HTMLSelectElement],
    gameWebsite: ["#gameswebsiteuri", HTMLInputElement],
    wikipedia: ["#wikipediauri", HTMLInputElement],
    giantbomb: ["#giantbomburi", HTMLInputElement],
    description: ["#album_desc", HTMLTextAreaElement],
    cover: ["#image", HTMLInputElement],
    addScreen: ["a[onclick^=AddScreenField]", HTMLAnchorElement],
};
const DOM = getDOMAccessor(elements);
function getScreenshotFields() {
    return document.querySelectorAll("#image_block > span > input[type=text]");
}

const requirementMap = {
    os: "OS",
    processor: "Processor",
    memory: "Memory",
    graphics: "Graphics",
    directx: "DirectX",
    storage: "Storage",
    notes: "Additional Notes",
};
/**
 * @param requirements - Game's system requirements.
 * @returns BBCode list of system requirements.
 */
function buildRequirementFields(requirements) {
    let fields = "";
    for (const [key, value] of Object.entries(requirements)) {
        if (value) {
            fields += `[*][b]${requirementMap[key]}[/b]: ${value}\n`;
        }
    }
    return fields.trim();
}
/**
 * @param requirements - Game's system requirements, minimum and recommended.
 * @returns Stringified system requirements in the customary box.
 */
function buildRequirements$1(requirements) {
    let string = "\n[quote][align=center][u][b]System Requirements[/b][/u][/align]\n";
    let interior = "";
    if (requirements.minimum) {
        interior += "\n[b]Minimum[/b]\n";
        interior += buildRequirementFields(requirements.minimum);
    }
    if (requirements.recommended) {
        interior += "\n[b]Recommended[/b]\n";
        interior += buildRequirementFields(requirements.recommended);
    }
    string += interior.length === 0 ? "\nNot available" : interior;
    string += "[/quote]";
    return string;
}
/**
 * @param game - Game's details.
 * @returns Complete description.
 */
function buildDescription(game) {
    let description = "[align=center][u][b]About the game[/b][/u][/align]\n";
    description += game.description;
    description += buildRequirements$1(game.requirements);
    return description;
}

/**
 * Fills screenshot fields with image links, adding fields in needed.
 * @param game - Game's details.
 */
function buildScreenshots(game) {
    let screens = getScreenshotFields();
    const neededScreenFields = Math.min(20, game.screenshots.length); // Up to 20 screenshots allowed.
    if (DOM.addScreenElement) {
        for (let i = 0; i < neededScreenFields - screens.length; i++) {
            DOM.addScreenElement.click();
        }
    }
    else {
        throw new Error("AddScreenField element not found");
    }
    screens = getScreenshotFields();
    if (screens.length < neededScreenFields) {
        throw new RangeError(`Screen field count mismatch. Wanted: ${neededScreenFields}; present: ${screens.length}`);
    }
    game.screenshots.forEach((screen, index) => {
        screens[index].value = screen;
    });
}
/**
 * Maps displayed rating to `Select` element's values.
 */
const ratingMap$1 = {
    "3+": "1",
    "7+": "3",
    "12+": "5",
    "16+": "7",
    "18+": "9",
    "N/A": "13",
};
/**
 * Fills the upload page with info about the game.
 * @param game - Game's details.
 */
function fillDetails(game) {
    DOM.title = game.title;
    DOM.aliases = game.aliases.join(", ");
    DOM.tags = game.tags.join(", ");
    DOM.year = game.year;
    DOM.rating = ratingMap$1[game.rating] ?? "";
    DOM.gameWebsite = game.links.gameWebsite ?? "";
    DOM.wikipedia = game.links.wikipedia ?? "";
    DOM.giantbomb = game.links.giantbomb ?? "";
    DOM.cover = game.coverImage;
    DOM.description = buildDescription(game);
    DOM.platform = game.platform;
    buildScreenshots(game);
}

/**
 * Inserts a new row on the upload page in the place suitable for uploady and fills it with provided data.
 * @param props - Optional properties.
 * @returns Content cell of the inserted row.
 */
function insertUploadyRow(props) {
    if (!DOM.titleRowElement) {
        throw new Error("Title row not found");
    }
    const row = document.createElement("tr");
    const label = document.createElement("td");
    const content = document.createElement("td");
    props.row_id ? (row.id = props.row_id) : void 0;
    label.className = "label";
    label.innerHTML = props.label ?? "";
    row.append(label, content);
    DOM.titleRowElement.before(row);
    return content;
}

const GGn = {
    fillDetails,
    insertUploadyRow,
};

/**
 * Builds aliases from product info.
 * @param product - Product info.
 * @returns A string array of aliases.
 */
function buildAliases(product) {
    const aliases = [product.ja.work_name, product.ja.workno];
    return aliases.filter((item) => item !== product.en.work_name);
}
const tagMap = new Map([
    ["ACN", "action"],
    ["QIZ", "quiz"],
    ["ADV", "adventure"],
    ["RPG", "role.playing.game"],
    ["TBL", "table"],
    ["DNV", "visual.novel"],
    ["SLN", "simulation"],
    ["TYP", "typing"],
    ["STG", undefined], // DLsite's "shooting" category is weird.
    ["PZL", "puzzle"],
    ["ETC", undefined], // Misc.
]);
/**
 * Maps DLsite's work types to GGn's tags.
 * @param product - Product info.
 * @returns A string array of tags.
 */
function buildTags(product) {
    const tags = [];
    if (product.age_category === 3) {
        tags.push("adult");
    }
    const workType = tagMap.get(product.work_type);
    if (workType) {
        tags.push(workType);
    }
    return tags;
}
function getYear(date) {
    return new Date(date).getFullYear().toString();
}
/**
 * Roughly maps DLsite's age categories to PEGI.
 */
const ratingMap = [null, "7+", "16+", "18+"];
/**
 * Maps DLsite's system requirements to generic format.
 * @param product - Product info.
 * @returns An object with system requirements.
 */
function buildRequirements(product) {
    return {
        minimum: {
            os: DLsite.formatMachineString(product.machine),
            processor: product.cpu,
            memory: product.memory,
            graphics: product.vram,
            directx: product.directx,
            storage: product.hdd,
            notes: product.etc,
        },
        recommended: null,
    };
}
function url_https(URL) {
    return URL.replace(/^\/\//, "https://");
}
/**
 * Converts DLsite specific data to generic format.
 * @param product - Multi-language product info.
 * @param description - Game's description. Optional.
 * @returns An object with game details.
 */
function productToGame(product, description) {
    return {
        title: product.en.work_name,
        aliases: buildAliases(product),
        tags: buildTags(product.en),
        year: getYear(product.en.regist_date),
        rating: ratingMap[product.en.age_category] ?? "N/A",
        links: {
            gameWebsite: DLsite.getGamePageUrl(product.en, null),
        },
        description: description ?? "",
        requirements: buildRequirements(product.en),
        platform: "Windows",
        coverImage: url_https(product.en.image_main?.url ?? ""),
        screenshots: product.en.image_samples?.map((i) => url_https(i.url)) ?? [],
    };
}

async function searchCode(code) {
    assertValidCode(code);
    console.debug(`Looking up ${code}...`);
    let redirected = false;
    const controller = new AbortController();
    const signal = controller.signal;
    const product = {};
    let description;
    const productPromise = {
        ja: DLsite.getProduct(code, "ja", signal),
        en: DLsite.getProduct(code, "en", signal),
    };
    const descriptionPromise = DLsite.getDescription(code, "en", signal);
    try {
        [product.ja, product.en, description] = await Promise.all([
            productPromise.ja,
            productPromise.en,
            descriptionPromise.catch((e) => {
                if (e instanceof Error && e.message === "Redirect") {
                    console.warn("Game page request was redirected. Retrying once more info is available");
                    redirected = true;
                    return "";
                }
                else {
                    throw e;
                }
            }),
        ]);
    }
    catch (e) {
        console.debug("Aborting all pending requests");
        controller.abort();
        throw e;
    }
    console.debug(`Found ${product.en.work_name}`);
    if (redirected) {
        description = await DLsite.getDescription(product.en, "en");
    }
    const game = productToGame(product, description);
    GGn.fillDetails(game);
    return {
        link: DLsite.getGamePageUrl(product.en, "en"),
        kana: product.ja.work_name_kana,
    };
}

/* src\uploady\dlsite\DLsiteUploady.svelte generated by Svelte v4.2.14 */

function add_css$1(target) {
	append_styles(target, "svelte-79w9i0", ".uploady-searchbar-area{display:flex;flex-direction:row}.uploady-searchbar-area>*{height:auto}.uploady-message{padding:0;margin:0 5px}");
}

// (68:4) {#if showMessage || messageHasContent}
function create_if_block_4(ctx) {
	let input;
	let input_value_value;
	let mounted;
	let dispose;

	return {
		c() {
			input = element("input");
			attr(input, "class", "uploady-message-toggle");
			attr(input, "type", "button");
			input.value = input_value_value = /*showMessage*/ ctx[6] ? "Hide" : "Show";
		},
		m(target, anchor) {
			insert(target, input, anchor);

			if (!mounted) {
				dispose = listen(input, "click", /*click_handler*/ ctx[11]);
				mounted = true;
			}
		},
		p(ctx, dirty) {
			if (dirty & /*showMessage*/ 64 && input_value_value !== (input_value_value = /*showMessage*/ ctx[6] ? "Hide" : "Show")) {
				input.value = input_value_value;
			}
		},
		d(detaching) {
			if (detaching) {
				detach(input);
			}

			mounted = false;
			dispose();
		}
	};
}

// (77:2) {#if showMessage}
function create_if_block(ctx) {
	let p;
	let p_transition;
	let current;

	function select_block_type(ctx, dirty) {
		if (!/*error*/ ctx[4]) return create_if_block_1;
		return create_else_block;
	}

	let current_block_type = select_block_type(ctx);
	let if_block = current_block_type(ctx);

	return {
		c() {
			p = element("p");
			if_block.c();
			attr(p, "class", "uploady-message");
		},
		m(target, anchor) {
			insert(target, p, anchor);
			if_block.m(p, null);
			current = true;
		},
		p(ctx, dirty) {
			if (current_block_type === (current_block_type = select_block_type(ctx)) && if_block) {
				if_block.p(ctx, dirty);
			} else {
				if_block.d(1);
				if_block = current_block_type(ctx);

				if (if_block) {
					if_block.c();
					if_block.m(p, null);
				}
			}
		},
		i(local) {
			if (current) return;

			if (local) {
				add_render_callback(() => {
					if (!current) return;
					if (!p_transition) p_transition = create_bidirectional_transition(p, slide, {}, true);
					p_transition.run(1);
				});
			}

			current = true;
		},
		o(local) {
			if (local) {
				if (!p_transition) p_transition = create_bidirectional_transition(p, slide, {}, false);
				p_transition.run(0);
			}

			current = false;
		},
		d(detaching) {
			if (detaching) {
				detach(p);
			}

			if_block.d();
			if (detaching && p_transition) p_transition.end();
		}
	};
}

// (88:6) {:else}
function create_else_block(ctx) {
	let div;
	let strong;
	let t;

	return {
		c() {
			div = element("div");
			strong = element("strong");
			t = text(/*error*/ ctx[4]);
			attr(strong, "class", "important_text");
			attr(div, "class", "dlsite-error");
		},
		m(target, anchor) {
			insert(target, div, anchor);
			append(div, strong);
			append(strong, t);
		},
		p(ctx, dirty) {
			if (dirty & /*error*/ 16) set_data(t, /*error*/ ctx[4]);
		},
		d(detaching) {
			if (detaching) {
				detach(div);
			}
		}
	};
}

// (79:6) {#if !error}
function create_if_block_1(ctx) {
	let t;
	let if_block1_anchor;
	let if_block0 = /*link*/ ctx[2] && create_if_block_3(ctx);
	let if_block1 = /*kana*/ ctx[3] && create_if_block_2(ctx);

	return {
		c() {
			if (if_block0) if_block0.c();
			t = space();
			if (if_block1) if_block1.c();
			if_block1_anchor = empty();
		},
		m(target, anchor) {
			if (if_block0) if_block0.m(target, anchor);
			insert(target, t, anchor);
			if (if_block1) if_block1.m(target, anchor);
			insert(target, if_block1_anchor, anchor);
		},
		p(ctx, dirty) {
			if (/*link*/ ctx[2]) {
				if (if_block0) {
					if_block0.p(ctx, dirty);
				} else {
					if_block0 = create_if_block_3(ctx);
					if_block0.c();
					if_block0.m(t.parentNode, t);
				}
			} else if (if_block0) {
				if_block0.d(1);
				if_block0 = null;
			}

			if (/*kana*/ ctx[3]) {
				if (if_block1) {
					if_block1.p(ctx, dirty);
				} else {
					if_block1 = create_if_block_2(ctx);
					if_block1.c();
					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
				}
			} else if (if_block1) {
				if_block1.d(1);
				if_block1 = null;
			}
		},
		d(detaching) {
			if (detaching) {
				detach(t);
				detach(if_block1_anchor);
			}

			if (if_block0) if_block0.d(detaching);
			if (if_block1) if_block1.d(detaching);
		}
	};
}

// (80:8) {#if link}
function create_if_block_3(ctx) {
	let div;
	let a;
	let t;

	return {
		c() {
			div = element("div");
			a = element("a");
			t = text(/*link*/ ctx[2]);
			attr(a, "href", /*link*/ ctx[2]);
			attr(a, "rel", "noreferrer noopener");
			attr(a, "target", "_blank");
			attr(div, "class", "dlsite-link");
		},
		m(target, anchor) {
			insert(target, div, anchor);
			append(div, a);
			append(a, t);
		},
		p(ctx, dirty) {
			if (dirty & /*link*/ 4) set_data(t, /*link*/ ctx[2]);

			if (dirty & /*link*/ 4) {
				attr(a, "href", /*link*/ ctx[2]);
			}
		},
		d(detaching) {
			if (detaching) {
				detach(div);
			}
		}
	};
}

// (85:8) {#if kana}
function create_if_block_2(ctx) {
	let div;
	let t0;
	let t1;

	return {
		c() {
			div = element("div");
			t0 = text("Kana title: ");
			t1 = text(/*kana*/ ctx[3]);
			attr(div, "class", "dlsite-kana");
		},
		m(target, anchor) {
			insert(target, div, anchor);
			append(div, t0);
			append(div, t1);
		},
		p(ctx, dirty) {
			if (dirty & /*kana*/ 8) set_data(t1, /*kana*/ ctx[3]);
		},
		d(detaching) {
			if (detaching) {
				detach(div);
			}
		}
	};
}

function create_fragment$1(ctx) {
	let div1;
	let div0;
	let input0;
	let t0;
	let input1;
	let t1;
	let t2;
	let mounted;
	let dispose;
	let if_block0 = (/*showMessage*/ ctx[6] || /*messageHasContent*/ ctx[7]) && create_if_block_4(ctx);
	let if_block1 = /*showMessage*/ ctx[6] && create_if_block(ctx);

	return {
		c() {
			div1 = element("div");
			div0 = element("div");
			input0 = element("input");
			t0 = space();
			input1 = element("input");
			t1 = space();
			if (if_block0) if_block0.c();
			t2 = space();
			if (if_block1) if_block1.c();
			attr(input0, "class", "uploady-searchbar");
			attr(input0, "type", "text");
			attr(input0, "size", "8");
			attr(input0, "placeholder", "DLsite code");
			attr(input1, "class", "uploady-search-button");
			attr(input1, "type", "button");
			input1.value = "Search";
			input1.disabled = /*searchDisabled*/ ctx[5];
			attr(div0, "class", "uploady-searchbar-area");
			attr(div1, "class", "uploady dlsite-uploady");
		},
		m(target, anchor) {
			insert(target, div1, anchor);
			append(div1, div0);
			append(div0, input0);
			set_input_value(input0, /*code*/ ctx[1]);
			/*input0_binding*/ ctx[10](input0);
			append(div0, t0);
			append(div0, input1);
			append(div0, t1);
			if (if_block0) if_block0.m(div0, null);
			append(div1, t2);
			if (if_block1) if_block1.m(div1, null);

			if (!mounted) {
				dispose = [
					listen(input0, "input", /*input0_input_handler*/ ctx[9]),
					listen(input1, "click", /*searchButtonClick*/ ctx[8])
				];

				mounted = true;
			}
		},
		p(ctx, [dirty]) {
			if (dirty & /*code*/ 2 && input0.value !== /*code*/ ctx[1]) {
				set_input_value(input0, /*code*/ ctx[1]);
			}

			if (dirty & /*searchDisabled*/ 32) {
				input1.disabled = /*searchDisabled*/ ctx[5];
			}

			if (/*showMessage*/ ctx[6] || /*messageHasContent*/ ctx[7]) {
				if (if_block0) {
					if_block0.p(ctx, dirty);
				} else {
					if_block0 = create_if_block_4(ctx);
					if_block0.c();
					if_block0.m(div0, null);
				}
			} else if (if_block0) {
				if_block0.d(1);
				if_block0 = null;
			}

			if (/*showMessage*/ ctx[6]) {
				if (if_block1) {
					if_block1.p(ctx, dirty);

					if (dirty & /*showMessage*/ 64) {
						transition_in(if_block1, 1);
					}
				} else {
					if_block1 = create_if_block(ctx);
					if_block1.c();
					transition_in(if_block1, 1);
					if_block1.m(div1, null);
				}
			} else if (if_block1) {
				group_outros();

				transition_out(if_block1, 1, 1, () => {
					if_block1 = null;
				});

				check_outros();
			}
		},
		i(local) {
			transition_in(if_block1);
		},
		o(local) {
			transition_out(if_block1);
		},
		d(detaching) {
			if (detaching) {
				detach(div1);
			}

			/*input0_binding*/ ctx[10](null);
			if (if_block0) if_block0.d();
			if (if_block1) if_block1.d();
			mounted = false;
			run_all(dispose);
		}
	};
}

function* set(value) {
	while (true) {
		yield value;
	}
}

function instance$1($$self, $$props, $$invalidate) {
	let searchbar;

	/** Value of the input. */
	let code = "";

	/** Link to the game's English page. */
	let link = "",
		/** Game's title in kana. */
		kana = "",
		/** Error message. */
		error = "";

	let searchDisabled = false;
	let showMessage = false;
	let messageHasContent = false;

	async function searchButtonClick() {
		$$invalidate(2, [link, kana, error] = set(""), link, $$invalidate(3, kana), $$invalidate(4, error));
		$$invalidate(6, showMessage = false);
		$$invalidate(5, searchDisabled = true);
		$$invalidate(7, messageHasContent = false);

		try {
			const result = await searchCode(code.trim().toUpperCase());
			$$invalidate(2, { link, kana } = result, link, $$invalidate(3, kana));
			searchbar.addEventListener("input", () => $$invalidate(5, searchDisabled = false), { once: true });
		} catch(err) {
			$$invalidate(5, searchDisabled = false);

			if (err instanceof Error) {
				$$invalidate(4, error = err.message);
				noty({ text: error, type: "error" });
			}

			throw err;
		} finally {
			$$invalidate(6, showMessage = true);
			$$invalidate(7, messageHasContent = true);
		}
	}

	function input0_input_handler() {
		code = this.value;
		$$invalidate(1, code);
	}

	function input0_binding($$value) {
		binding_callbacks[$$value ? 'unshift' : 'push'](() => {
			searchbar = $$value;
			$$invalidate(0, searchbar);
		});
	}

	const click_handler = () => $$invalidate(6, showMessage = !showMessage);

	return [
		searchbar,
		code,
		link,
		kana,
		error,
		searchDisabled,
		showMessage,
		messageHasContent,
		searchButtonClick,
		input0_input_handler,
		input0_binding,
		click_handler
	];
}

class DLsiteUploady extends SvelteComponent {
	constructor(options) {
		super();
		init(this, options, instance$1, create_fragment$1, safe_not_equal, {}, add_css$1);
	}
}

/* src\uploady\Uploady.svelte generated by Svelte v4.2.14 */

function add_css(target) {
	append_styles(target, "svelte-p5n2vh", "input[type=\"button\"]:disabled{background-color:#767676;box-shadow:none;color:#d5d4d4}");
}

function get_each_context(ctx, list, i) {
	const child_ctx = ctx.slice();
	child_ctx[5] = list[i];
	return child_ctx;
}

// (16:4) {#each enabledUploadyList as id}
function create_each_block(ctx) {
	let option;
	let t_value = /*uploadyMap*/ ctx[2][/*id*/ ctx[5]].name + "";
	let t;

	return {
		c() {
			option = element("option");
			t = text(t_value);
			option.__value = /*id*/ ctx[5];
			set_input_value(option, option.__value);
		},
		m(target, anchor) {
			insert(target, option, anchor);
			append(option, t);
		},
		p: noop,
		d(detaching) {
			if (detaching) {
				detach(option);
			}
		}
	};
}

function create_fragment(ctx) {
	let div;
	let select;
	let t;
	let switch_instance;
	let current;
	let mounted;
	let dispose;
	let each_value = ensure_array_like(/*enabledUploadyList*/ ctx[3]);
	let each_blocks = [];

	for (let i = 0; i < each_value.length; i += 1) {
		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
	}

	var switch_value = /*activeUploady*/ ctx[1];

	function switch_props(ctx, dirty) {
		return {};
	}

	if (switch_value) {
		switch_instance = construct_svelte_component(switch_value, switch_props());
	}

	return {
		c() {
			div = element("div");
			select = element("select");

			for (let i = 0; i < each_blocks.length; i += 1) {
				each_blocks[i].c();
			}

			t = space();
			if (switch_instance) create_component(switch_instance.$$.fragment);
			attr(select, "class", "uploady-select");
			attr(select, "name", "uploady-select");
			if (/*activeUploadyId*/ ctx[0] === void 0) add_render_callback(() => /*select_change_handler*/ ctx[4].call(select));
			attr(div, "class", "uploady-wrapper");
		},
		m(target, anchor) {
			insert(target, div, anchor);
			append(div, select);

			for (let i = 0; i < each_blocks.length; i += 1) {
				if (each_blocks[i]) {
					each_blocks[i].m(select, null);
				}
			}

			select_option(select, /*activeUploadyId*/ ctx[0], true);
			append(div, t);
			if (switch_instance) mount_component(switch_instance, div, null);
			current = true;

			if (!mounted) {
				dispose = listen(select, "change", /*select_change_handler*/ ctx[4]);
				mounted = true;
			}
		},
		p(ctx, [dirty]) {
			if (dirty & /*enabledUploadyList, uploadyMap*/ 12) {
				each_value = ensure_array_like(/*enabledUploadyList*/ ctx[3]);
				let i;

				for (i = 0; i < each_value.length; i += 1) {
					const child_ctx = get_each_context(ctx, each_value, i);

					if (each_blocks[i]) {
						each_blocks[i].p(child_ctx, dirty);
					} else {
						each_blocks[i] = create_each_block(child_ctx);
						each_blocks[i].c();
						each_blocks[i].m(select, null);
					}
				}

				for (; i < each_blocks.length; i += 1) {
					each_blocks[i].d(1);
				}

				each_blocks.length = each_value.length;
			}

			if (dirty & /*activeUploadyId, enabledUploadyList*/ 9) {
				select_option(select, /*activeUploadyId*/ ctx[0]);
			}

			if (dirty & /*activeUploady*/ 2 && switch_value !== (switch_value = /*activeUploady*/ ctx[1])) {
				if (switch_instance) {
					group_outros();
					const old_component = switch_instance;

					transition_out(old_component.$$.fragment, 1, 0, () => {
						destroy_component(old_component, 1);
					});

					check_outros();
				}

				if (switch_value) {
					switch_instance = construct_svelte_component(switch_value, switch_props());
					create_component(switch_instance.$$.fragment);
					transition_in(switch_instance.$$.fragment, 1);
					mount_component(switch_instance, div, null);
				} else {
					switch_instance = null;
				}
			}
		},
		i(local) {
			if (current) return;
			if (switch_instance) transition_in(switch_instance.$$.fragment, local);
			current = true;
		},
		o(local) {
			if (switch_instance) transition_out(switch_instance.$$.fragment, local);
			current = false;
		},
		d(detaching) {
			if (detaching) {
				detach(div);
			}

			destroy_each(each_blocks, detaching);
			if (switch_instance) destroy_component(switch_instance);
			mounted = false;
			dispose();
		}
	};
}

function instance($$self, $$props, $$invalidate) {
	let activeUploady;

	const uploadyMap = {
		dlsite: { name: "DLsite", uploady: DLsiteUploady }
	};

	const enabledUploadyList = ["dlsite"];
	let activeUploadyId = enabledUploadyList[0];

	function select_change_handler() {
		activeUploadyId = select_value(this);
		$$invalidate(0, activeUploadyId);
		$$invalidate(3, enabledUploadyList);
	}

	$$self.$$.update = () => {
		if ($$self.$$.dirty & /*activeUploadyId*/ 1) {
			$$invalidate(1, activeUploady = activeUploadyId
			? uploadyMap[activeUploadyId].uploady
			: null);
		}
	};

	return [
		activeUploadyId,
		activeUploady,
		uploadyMap,
		enabledUploadyList,
		select_change_handler
	];
}

class Uploady extends SvelteComponent {
	constructor(options) {
		super();
		init(this, options, instance, create_fragment, safe_not_equal, {}, add_css);
	}
}

const target = GGn.insertUploadyRow({
    row_id: `muploady_tr`,
    label: "mUploady",
});
new Uploady({ target });

})(DOMPurify);
