/* eslint-disable @typescript-eslint/ban-types */
/**
 * Adapted from {@link https://github.com/needim/noty/blob/master/index.d.ts}
 */
declare namespace Noty {
  type Type =
    | "alert"
    | "success"
    | "warning"
    | "error"
    | "info"
    | "information";
  type Theme = string;
  type Layout =
    | "top"
    | "topLeft"
    | "topCenter"
    | "topRight"
    | "center"
    | "centerLeft"
    | "centerRight"
    | "bottom"
    | "bottomLeft"
    | "bottomCenter"
    | "bottomRight";
  type Event =
    | "beforeShow"
    | "onShow"
    | "afterShow"
    | "onClose"
    | "afterClose"
    | "onHover"
    | "onTemplate"
    | "onClick";

  interface Button {
    new (
      text: string,
      classNames: string,
      cb: Function,
      attributes: any,
    ): Noty.Button;
  }

  interface Options {
    type?: Noty.Type;
    layout?: Noty.Layout;
    theme?: Noty.Theme;
    text?: string;
    timeout?: false | number;
    progressBar?: boolean;
    closeWith?: ("click" | "button")[];
    animation?: {
      open?: string | null | Function;
      close?: string | null | Function;
    };
    id?: false | string;
    force?: boolean;
    killer?: boolean | string;
    queue?: string;
    container?: false | string;
    buttons?: Noty.Button[];
    callbacks?: {
      beforeShow?: () => void;
      onShow?: () => void;
      afterShow?: () => void;
      onClose?: () => void;
      afterClose?: () => void;
      onHover?: () => void;
      onTemplate?: () => void;
      onClick?: () => void;
    };
    sounds?: {
      sources?: string[];
      volume?: number;
      conditions?: string[];
    };
    docTitle?: {
      conditions?: string[];
    };
    modal?: boolean;
  }
}

declare function noty(options: Noty.Options): void;
