import { defineConfig } from "rollup";
import resolve from "@rollup/plugin-node-resolve";
import svelte from "rollup-plugin-svelte";
import autoPreprocess from "svelte-preprocess";
import typescript from "@rollup/plugin-typescript";
import pkg from "./package.json" with { type: "json" };
import meta from "./meta";

process.env.BUILD ??= "prod";
const plugins = [
  resolve({
    browser: true,
    exportConditions: ["svelte"],
    extensions: [".svelte"],
  }),
  svelte({
    emitCss: false,
    preprocess: autoPreprocess({
      replace: [["process.env.BUILD", `"${process.env.BUILD}"`]],
    }),
  }),
  typescript(),
];

export default defineConfig({
  input: "./src/index.ts",
  external: ["dompurify"],
  output: {
    file: pkg.main,
    banner: meta,
    format: "iife",
    globals: { dompurify: "DOMPurify" },
    generatedCode: "es2015",
    externalLiveBindings: false,
    esModule: false,
    interop: "default",
    indent: false,
  },
  plugins,
});
