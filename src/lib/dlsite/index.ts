import { getProduct } from "./getProduct";
import { getDescription } from "./description/getDescription";
import { getGamePageUrl } from "./utils/getGamePageUrl";
import { formatMachineString } from "./utils/formatMachineString";

export default {
  getProduct,
  getDescription,
  getGamePageUrl,
  formatMachineString,
};

/** Assert methods are broken.
 * @see {@link https://github.com/microsoft/TypeScript/issues/36931}
 */
export { assertValidCode } from "./utils/assertValidCode";
