import type { DLsiteCode } from "../types";

const DLsiteCodeRegex = /^(RJ|RE|VJ|BJ)(\d{4}|\d{6}|\d{8})$/i;

/**
 * Throws an error if the code is not in the proper format.
 * @param code - A string to assert as DLsite product ID.
 */
export function assertValidCode(code: string): asserts code is DLsiteCode {
  if (!DLsiteCodeRegex.test(code)) {
    console.debug("Bad code: %s", code);
    throw new Error("Bad code format");
  }
}
