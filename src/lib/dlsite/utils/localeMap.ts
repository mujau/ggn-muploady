import type { Locale } from "../types";

type LocaleMap_t = {
  [locale in Locale]: string;
};
export const LocaleMap: LocaleMap_t = {
  "ja": "ja_JP",
  "zh-TW": "zh-TW",
  "zh-CN": "zh-CN",
  "ko": "ko_KR",
  "en": "en_US",
};
