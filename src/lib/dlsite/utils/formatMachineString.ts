const windowsMap = new Map([
  ["W95", "95"],
  ["W98", "98"],
  ["WME", "Me"],
  ["W20", "2000"],
  ["WXP", "XP"],
  ["VIS", "Vista"],
  ["W07", "7"],
  ["W08", "8"],
  ["W81", "8.1"],
  ["W10", "10"],
  ["W11", "11"], // future proofing
]);

const Pattern = {
  MacOS: /^MC.$/,
  Android: /^AND(\d{1,3}(?:\.\d{1,3}){0,2})\+$/,
  iOS: /^IOS(\d{1,3}(?:\.\d{1,3}){0,2})\+$/,
};

interface OS {
  Windows: string[];
  MacOS: boolean;
  Android: string;
  iOS: string;
  Other: string[];
}

/**
 * Parses OS compatibility string to an object.
 * @param machine - A string listing systems the product is compatible with.
 * @returns An object with OS compatibility info.
 */
function parseMachineString(machine: string) {
  const OS: OS = {
    Windows: [],
    MacOS: false,
    Android: "",
    iOS: "",
    Other: [],
  };
  for (const system of machine.split("#")) {
    if (!system) {
      continue;
    }
    if (windowsMap.has(system)) {
      OS.Windows.push(windowsMap.get(system)!);
    } else if (Pattern.MacOS.test(system)) {
      OS.MacOS = true;
    } else {
      const found = (["Android", "iOS"] as const).find((mobile) => {
        const match = Pattern[mobile].exec(system);
        if (match) {
          OS[mobile] = `${mobile} ${match[1]}+`;
          return true;
        }
      });
      if (!found) {
        OS.Other.push(system);
      }
    }
  }
  return OS;
}

/**
 * Converts DLsite's OS compatibility list to something less ugly.
 * @param machine - A string listing systems the product is compatible with.
 * @returns A human friendly OS compatibility list.
 */
export function formatMachineString(machine: string | null): string | null {
  if (!machine) {
    return null;
  }

  const OS = parseMachineString(machine);
  const parts: string[] = [];
  if (OS.Windows.length > 0) {
    parts.push("Windows " + OS.Windows.join(" / "));
  }
  if (OS.MacOS) {
    parts.push("MacOS");
  }
  if (OS.Android) {
    parts.push(OS.Android);
  }
  if (OS.iOS) {
    parts.push(OS.iOS);
  }
  if (OS.Other.length > 0) {
    parts.push(OS.Other.join(" | "));
  }
  return parts.join(" | ");
}
