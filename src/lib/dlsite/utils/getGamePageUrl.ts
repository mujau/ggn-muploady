import { LocaleMap } from "./localeMap";
import type { DLsiteCode, Locale, Product } from "../types";

const CodeToSiteMap = new Map([
  ["RJ", "maniax"],
  ["RE", "maniax"], // ecchi_eng is deprecated
  ["VJ", "pro"],
  ["BJ", "books"],
]);

/**
 * Gathers info needed for building product url.
 * @param product - Product info object or product id.
 * @returns An object with `site_id` and `workno` properties.
 */
function resolve<P extends Product | DLsiteCode>(product: P) {
  if (typeof product === "string") {
    const firstTwo = product.slice(0, 2);
    const site_id = CodeToSiteMap.get(firstTwo) ?? "home"; // Home should work anyway.
    return { site_id, workno: product as DLsiteCode };
  } else {
    return product as Product;
  }
}

/**
 * Builds DLsite product url from product data or product id.
 * @remarks Id resolution is less accurate, but can be executed asynchronously.
 * @param product - Product info object or product id.
 * @param locale - Page's language.
 * @returns Product url.
 */
export function getGamePageUrl<P extends Product | DLsiteCode>(
  product: P,
  locale: Locale | null,
): string {
  const { site_id, workno } = resolve(product);
  const query = locale ? `/?locale=${LocaleMap[locale]}` : "";
  return `https://www.dlsite.com/${site_id}/work/=/product_id/${workno}.html${query}`;
}
