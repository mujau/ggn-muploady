import { promisingXHR } from "utils/promisingXHR";
import { LocaleMap } from "./utils/localeMap";
import type { CompleteProduct, Locale, DLsiteCode } from "./types";

const ProductFields = [
  "age_category",
  "age_category_string",
  "maker_name",
  "machine",
  "cpu",
  "directx",
  "etc",
  "hdd",
  "memory",
  "vram",
  "regist_date",
  "site_id",
  "workno",
  "image_main",
  "image_samples",
  "work_name",
  "work_name_kana",
  "work_type",
  "work_type_string",
] as const;

export type Product = Pick<CompleteProduct, typeof ProductFields[number]>;

/**
 * Queries DLsite's API for product details.
 * @param code - Product ID.
 * @param locale - Response's language.
 * @param signal - Abort signal.
 * @returns Product promise.
 */
export async function getProduct(
  code: DLsiteCode,
  locale: Locale | null,
  signal: AbortSignal,
): Promise<Product> {
  const URL = "https://www.dlsite.com/home/api/=/product.json";
  const headers = { "X-Requested-With": "XMLHttpRequest" };
  const searchParams = new URLSearchParams({
    workno: code,
    is_ana: "0",
    fields: ProductFields.join(","),
  });
  if (locale) {
    searchParams.append("locale", LocaleMap[locale]);
  }
  const fullURL = URL + "?" + searchParams.toString();
  const { status, responseText } = await promisingXHR(
    {
      headers: headers,
      method: "GET",
      url: fullURL,
      anonymous: true,
    },
    signal,
  );
  if (status < 200 || status > 299) {
    console.debug("Bad response body: %s", responseText);
    throw new Error(`Bad response status: ${status}`);
  }
  const json = JSON.parse(responseText);
  if (!Array.isArray(json)) {
    console.debug("Not an array: %o", json);
    throw new Error("API response is expected to be an array");
  }
  if (!json.length) {
    console.debug("Empty response from %s", fullURL);
    throw new Error("Empty API response");
  }
  if (json.length !== 1) {
    console.debug("More than one result: %o", json);
    throw new Error("The API returned more than one result");
  }
  const product = json[0];
  return product;
}
