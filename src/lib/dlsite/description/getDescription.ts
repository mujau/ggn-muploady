import { type QueryReplacerMap, queryReplacer } from "utils/queryReplacer";
import type { DLsiteCode, Locale, Product } from "../types";
import { DLsite2BB } from "./DLsite2BB";
import { getGamePage } from "./getGamePage";

const htmlToBBcodeMap: QueryReplacerMap = {
  "p": ({ innerHTML }) => `\n\n${innerHTML}\n\n`,
  "h1, h2, h3, h4, h5": ({ innerHTML }) => `\n[b]${innerHTML}[/b]\n`,
  "br": () => "\n",
  "a": () => "",
} as const;

/**
 * Clean ups game's description and converts it to BBcode.
 * @param desc - A string containing game's description.
 * @returns A string containing game's cleaned up description converted to BBcode.
 */
export function cleanDescription(desc: string) {
  /** Nuke existing line breaks to make place for the ones parsed from <br> tags. */
  desc = desc.replace(/\n/g, "");
  const parser = new DOMParser();
  const descDoc = parser.parseFromString(desc, "text/html");
  queryReplacer(descDoc, htmlToBBcodeMap, "outerHTML"); // HTML tags to BBcode.
  return DLsite2BB(descDoc.body.textContent ?? ""); // Other patterns and cleanup.
}

/**
 * Extracts game's description from the DOM.
 * @param doc - Game's page.
 * @returns A string containing game's description.
 */
function extractDescription(
  doc: Document | DocumentFragment | Element,
): string {
  const parts = doc.querySelectorAll(".work_parts");
  let desc = "";
  for (let i = 0; i < parts.length; i++) {
    desc += parts[i].innerHTML;
  }
  return desc;
}

/**
 * Fetches game's page and returns its description.
 * @param product - Product id or product info object.
 * @param locale - Page's language.
 * @param signal - Abort signal.
 * @returns A promise of game's description.
 */
export async function getDescription(
  product: DLsiteCode | Product,
  locale: Locale | null,
  signal?: AbortSignal,
): Promise<string> {
  const doc = await getGamePage(product, locale, signal);
  const desc = extractDescription(doc);
  return cleanDescription(desc);
}
