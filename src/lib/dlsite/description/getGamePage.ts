import DOMPurify from "dompurify";
import { promisingXHR } from "utils/promisingXHR";
import { getGamePageUrl } from "../utils/getGamePageUrl";
import type { DLsiteCode, Locale, Product } from "../types";

/**
 * Fetches game's page and returns it as a sanitized DocumentFragment.
 * @param product - Product id or product info object.
 * @param locale - Page's language.
 * @param signal - Abort signal.
 * @returns A clean DocumentFragment.
 */
export async function getGamePage(
  product: DLsiteCode | Product,
  locale: Locale | null,
  signal?: AbortSignal,
): Promise<DocumentFragment> {
  const url = getGamePageUrl(product, locale);

  const { responseText, status, finalUrl } = await promisingXHR(
    {
      method: "GET",
      url: url,
      anonymous: true,
    },
    signal,
  );

  if (status === 404) {
    throw new Error("No related products");
  }
  if (status < 200 || status > 299) {
    throw new Error(`Bad response status: ${status}`);
  }
  if (url !== finalUrl) {
    throw new Error("Redirect");
  }

  const clean = DOMPurify.sanitize(responseText, {
    FORBID_TAGS: ["img"],
    KEEP_CONTENT: true,
    RETURN_DOM_FRAGMENT: true,
  });
  return clean;
}
