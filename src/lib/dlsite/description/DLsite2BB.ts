const trimWhitespaceMap = new Map([
  [/^ +| +$/gm, ""], // Trim leading and trailing spaces (per line).
  [/ {2,}/g, " "], // Collapse multiple spaces.
  [/\n{3,}/g, "\n\n"], // Collapse more than two linebreaks (one empty line).
  [/(\[\/b\]\n)\n+/g, "$1"], // Collapse linebreaks after [b] tags.
]);

/**
 * Trims various whitespace patterns.
 * @param str - A string for trimming.
 * @returns A trimmed string.
 */
function trimWhitespace(str: string): string {
  for (const [regex, replacement] of trimWhitespaceMap) {
    str = str.replace(regex, replacement);
  }
  str = str.trim();
  str += "\n"; // Terminate the string with a newline.
  return str;
}

/**
 * Patterns for extracting data from list items.
 */
const listItemMatcherMap = {
  "-": /^- *(.+)/gm,
  "*": /^\* *(.+)/gm,
  "digit": /^\d{1,2}\. *(.+)/gm,
};

/**
 * Patterns for finding lists.
 */
const listMatcherMap = {
  "-": /(?:^(-).+\n(?:^(?!\1).+\n)?){2,}/gm, // The optional part allows it to consume a single plain line.
  "*": /(?:^(\*).+\n(?:^(?!\1).+\n)?){2,}/gm,
  "digit": /(?:^(\d{1,2})\..+\n(?:^(?!\1).+\n)?){2,}/gm,
};

type ListBullet = keyof typeof listItemMatcherMap;

/**
 * Transforms all list items to BBcode form.
 * @param list - A string containing all list items.
 * @param bullet - The symbol used as a bullet by the list.
 * @returns A string containing BBcodified list.
 */
function listReplacer(list: string, bullet: ListBullet): string {
  return list.replace(listItemMatcherMap[bullet], "[*]$1");
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const DLsite2BBMap = new Map<RegExp, any>([
  [/^\[([^\]]+)\]$\n{0,3}/gm, "\n[b]$1[/b]\n"], // [foo] header kind.
  [/^\* *(.+?) *\*$\n{0,3}/gm, "\n[b]$1[/b]\n"], // * foo * header kind.
  [listMatcherMap["-"], listReplacer],
  [listMatcherMap["*"], listReplacer],
  [listMatcherMap["digit"], (list: string) => listReplacer(list, "digit")],
  [/([^\n]+[.,!?;])\n(?!\n|\[\*\])/g, "$1 "], // Single linebreak purging for lines terminated with punctuation. Excludes list items.
]);

/**
 * Trims the string and tries to replace recognizable patterns with BBcode.
 * @param str - A string to be BBcodified.
 * @returns A BBcodified string.
 */
export function DLsite2BB(str: string): string {
  str = trimWhitespace(str);
  for (const [regex, replacement] of DLsite2BBMap) {
    str = str.replace(regex, replacement);
  }
  return trimWhitespace(str);
}
