import { DOM } from "./DOM";

/**
 * Inserts a new row on the upload page in the place suitable for uploady and fills it with provided data.
 * @param props - Optional properties.
 * @returns Content cell of the inserted row.
 */
export function insertUploadyRow(props: {
  row_id?: string;
  label?: string;
}): HTMLTableCellElement {
  if (!DOM.titleRowElement) {
    throw new Error("Title row not found");
  }
  const row = document.createElement("tr");
  const label = document.createElement("td");
  const content = document.createElement("td");
  props.row_id ? (row.id = props.row_id) : void 0;
  label.className = "label";
  label.innerHTML = props.label ?? "";
  row.append(label, content);
  DOM.titleRowElement.before(row);
  return content;
}
