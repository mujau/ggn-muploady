import { fillDetails } from "./fillDetails";
import { insertUploadyRow } from "./insertUploadyRow";

export default {
  fillDetails,
  insertUploadyRow,
};
