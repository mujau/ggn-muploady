import { getDOMAccessor } from "utils/DOMAccessor";

const elements = {
  titleRow: ["#title_tr", HTMLTableRowElement],
  title: ["#title", HTMLInputElement],
  platform: ["#platform", HTMLSelectElement],
  aliases: ["#aliases", HTMLInputElement],
  tags: ["#tags", HTMLInputElement],
  year: ["#year", HTMLInputElement],
  rating: ["#Rating", HTMLSelectElement],
  gameWebsite: ["#gameswebsiteuri", HTMLInputElement],
  wikipedia: ["#wikipediauri", HTMLInputElement],
  giantbomb: ["#giantbomburi", HTMLInputElement],
  description: ["#album_desc", HTMLTextAreaElement],
  cover: ["#image", HTMLInputElement],
  addScreen: ["a[onclick^=AddScreenField]", HTMLAnchorElement],
} as const;

export const DOM = getDOMAccessor(elements);

export function getScreenshotFields(): NodeListOf<HTMLInputElement> {
  return document.querySelectorAll<HTMLInputElement>(
    "#image_block > span > input[type=text]",
  );
}
