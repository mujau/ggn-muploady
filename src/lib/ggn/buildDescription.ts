import type { Game, Requirements, RequirementsMinRec } from "./types";

const requirementMap = {
  os: "OS",
  processor: "Processor",
  memory: "Memory",
  graphics: "Graphics",
  directx: "DirectX",
  storage: "Storage",
  notes: "Additional Notes",
} as const;

type RequirementTuple = [keyof Requirements, string | null];

/**
 * @param requirements - Game's system requirements.
 * @returns BBCode list of system requirements.
 */
function buildRequirementFields(requirements: Requirements): string {
  let fields = "";
  for (const [key, value] of Object.entries(
    requirements,
  ) as RequirementTuple[]) {
    if (value) {
      fields += `[*][b]${requirementMap[key]}[/b]: ${value}\n`;
    }
  }
  return fields.trim();
}

/**
 * @param requirements - Game's system requirements, minimum and recommended.
 * @returns Stringified system requirements in the customary box.
 */
function buildRequirements(requirements: RequirementsMinRec): string {
  let string =
    "\n[quote][align=center][u][b]System Requirements[/b][/u][/align]\n";
  let interior = "";
  if (requirements.minimum) {
    interior += "\n[b]Minimum[/b]\n";
    interior += buildRequirementFields(requirements.minimum);
  }
  if (requirements.recommended) {
    interior += "\n[b]Recommended[/b]\n";
    interior += buildRequirementFields(requirements.recommended);
  }

  string += interior.length === 0 ? "\nNot available" : interior;
  string += "[/quote]";
  return string;
}

/**
 * @param game - Game's details.
 * @returns Complete description.
 */
export function buildDescription(game: Game): string {
  let description = "[align=center][u][b]About the game[/b][/u][/align]\n";
  description += game.description;
  description += buildRequirements(game.requirements);
  return description;
}
