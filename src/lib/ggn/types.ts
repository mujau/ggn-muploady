export interface Requirements {
  os: string | null;
  processor: string | null;
  memory: string | null;
  graphics: string | null;
  directx: string | null;
  storage: string | null;
  notes: string | null;
}

export interface RequirementsMinRec {
  minimum: Requirements | null;
  recommended: Requirements | null;
}

export type Rating = "3+" | "7+" | "12+" | "16+" | "18+" | "N/A";

interface Links {
  gameWebsite?: string;
  wikipedia?: string;
  giantbomb?: string;
  vndb?: string;
  howlongtobeat?: string;
  amazon?: string;
  gamefaqs?: string;
  steam?: string;
  gog?: string;
  humblebundlestore?: string;
  itchio?: string;
  pcgamingwiki?: string;
  epicgames?: string;
  nexusmods?: string;
  mobygames?: string;
}

type Platform = "Windows" | "Linux" | "Mac" | "Android" | "iOS";

export interface Game {
  title: string;
  aliases: string[];
  tags: string[];
  year: string;
  rating: Rating;
  links: Links;
  description: string;
  requirements: RequirementsMinRec;
  platform: Platform;
  coverImage: string;
  screenshots: string[];
}
