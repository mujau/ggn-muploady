import type { Game, Rating } from "./types";
import { DOM, getScreenshotFields } from "./DOM";
import { buildDescription } from "./buildDescription";

/**
 * Fills screenshot fields with image links, adding fields in needed.
 * @param game - Game's details.
 */
function buildScreenshots(game: Game): void {
  let screens = getScreenshotFields();
  const neededScreenFields = Math.min(20, game.screenshots.length); // Up to 20 screenshots allowed.
  if (DOM.addScreenElement) {
    for (let i = 0; i < neededScreenFields - screens.length; i++) {
      DOM.addScreenElement.click();
    }
  } else {
    throw new Error("AddScreenField element not found");
  }

  screens = getScreenshotFields();
  if (screens.length < neededScreenFields) {
    throw new RangeError(
      `Screen field count mismatch. Wanted: ${neededScreenFields}; present: ${screens.length}`,
    );
  }

  game.screenshots.forEach((screen, index) => {
    screens[index].value = screen;
  });
}

type RatingMap = {
  [rating in Rating]: string;
};

/**
 * Maps displayed rating to `Select` element's values.
 */
const ratingMap: RatingMap = {
  "3+": "1",
  "7+": "3",
  "12+": "5",
  "16+": "7",
  "18+": "9",
  "N/A": "13",
} as const;

/**
 * Fills the upload page with info about the game.
 * @param game - Game's details.
 */
export function fillDetails(game: Game): void {
  DOM.title = game.title;
  DOM.aliases = game.aliases.join(", ");
  DOM.tags = game.tags.join(", ");
  DOM.year = game.year;
  DOM.rating = ratingMap[game.rating] ?? "";
  DOM.gameWebsite = game.links.gameWebsite ?? "";
  DOM.wikipedia = game.links.wikipedia ?? "";
  DOM.giantbomb = game.links.giantbomb ?? "";
  DOM.cover = game.coverImage;
  DOM.description = buildDescription(game);
  DOM.platform = game.platform;
  buildScreenshots(game);
}
