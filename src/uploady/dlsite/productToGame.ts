import DLsite from "lib/dlsite";
import type { Product_JaEn, Product } from "lib/dlsite/types";
import type { Game, RequirementsMinRec } from "lib/ggn/types";

/**
 * Builds aliases from product info.
 * @param product - Product info.
 * @returns A string array of aliases.
 */
function buildAliases(product: Product_JaEn): string[] {
  const aliases = [product.ja.work_name, product.ja.workno];
  return aliases.filter((item) => item !== product.en.work_name);
}

const tagMap = new Map([
  ["ACN", "action"],
  ["QIZ", "quiz"],
  ["ADV", "adventure"],
  ["RPG", "role.playing.game"],
  ["TBL", "table"],
  ["DNV", "visual.novel"],
  ["SLN", "simulation"],
  ["TYP", "typing"],
  ["STG", undefined], // DLsite's "shooting" category is weird.
  ["PZL", "puzzle"],
  ["ETC", undefined], // Misc.
]);

/**
 * Maps DLsite's work types to GGn's tags.
 * @param product - Product info.
 * @returns A string array of tags.
 */
function buildTags(product: Product): string[] {
  const tags = [];
  if (product.age_category === 3) {
    tags.push("adult");
  }

  const workType = tagMap.get(product.work_type);
  if (workType) {
    tags.push(workType);
  }

  return tags;
}

function getYear(date: string) {
  return new Date(date).getFullYear().toString();
}

/**
 * Roughly maps DLsite's age categories to PEGI.
 */
const ratingMap = [null, "7+", "16+", "18+"] as const;

/**
 * Maps DLsite's system requirements to generic format.
 * @param product - Product info.
 * @returns An object with system requirements.
 */
function buildRequirements(product: Product): RequirementsMinRec {
  return {
    minimum: {
      os: DLsite.formatMachineString(product.machine),
      processor: product.cpu,
      memory: product.memory,
      graphics: product.vram,
      directx: product.directx,
      storage: product.hdd,
      notes: product.etc,
    },
    recommended: null,
  };
}

function url_https(URL: string): string {
  return URL.replace(/^\/\//, "https://");
}

/**
 * Converts DLsite specific data to generic format.
 * @param product - Multi-language product info.
 * @param description - Game's description. Optional.
 * @returns An object with game details.
 */
export function productToGame(
  product: Product_JaEn,
  description?: string,
): Game {
  return {
    title: product.en.work_name,
    aliases: buildAliases(product),
    tags: buildTags(product.en),
    year: getYear(product.en.regist_date),
    rating: ratingMap[product.en.age_category] ?? "N/A",
    links: {
      gameWebsite: DLsite.getGamePageUrl(product.en, null),
    },
    description: description ?? "",
    requirements: buildRequirements(product.en),
    platform: "Windows",
    coverImage: url_https(product.en.image_main?.url ?? ""),
    screenshots: product.en.image_samples?.map((i) => url_https(i.url)) ?? [],
  };
}
