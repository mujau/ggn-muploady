import DLsite, { assertValidCode } from "lib/dlsite";
import type { Product_JaEn } from "lib/dlsite/types";
import GGn from "lib/ggn";
import { productToGame } from "./productToGame";

export async function searchCode(code: string) {
  assertValidCode(code);
  console.debug(`Looking up ${code}...`);

  let redirected = false;
  const controller = new AbortController();
  const signal = controller.signal;

  const product = {} as Product_JaEn;
  let description: string;
  const productPromise = {
    ja: DLsite.getProduct(code, "ja", signal),
    en: DLsite.getProduct(code, "en", signal),
  };
  const descriptionPromise = DLsite.getDescription(code, "en", signal);

  try {
    [product.ja, product.en, description] = await Promise.all([
      productPromise.ja,
      productPromise.en,
      descriptionPromise.catch((e) => {
        if (e instanceof Error && e.message === "Redirect") {
          console.warn(
            "Game page request was redirected. Retrying once more info is available",
          );
          redirected = true;
          return "";
        } else {
          throw e;
        }
      }),
    ]);
  } catch (e) {
    console.debug("Aborting all pending requests");
    controller.abort();
    throw e;
  }
  console.debug(`Found ${product.en.work_name}`);

  if (redirected) {
    description = await DLsite.getDescription(product.en, "en");
  }

  const game = productToGame(product, description);
  GGn.fillDetails(game);

  return {
    link: DLsite.getGamePageUrl(product.en, "en"),
    kana: product.ja.work_name_kana,
  };
}
