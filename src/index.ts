import "svelte";
import Uploady from "./uploady/Uploady.svelte";
import GGn from "./lib/ggn";

const target = GGn.insertUploadyRow({
  row_id: `muploady_tr`,
  label: "mUploady",
});
new Uploady({ target });
