/**
 * The key is a selector string.
 * The value is a function receiving an `Element`
 * and returning what should become its `innerHTML`/`outerHTML`.
 */
export interface QueryReplacerMap {
  [selectors: string]: (element: Element) => string;
}

/**
 * Performs replacements on descendants of the provided element.
 * @param doc - An object compatible with `querySelectorAll`.
 * @param replacements - An object mapping selectors to replacer functions.
 * @param target - The property that should be replaced. `outerHTML` by default.
 */
export function queryReplacer(
  doc: Document | DocumentFragment | Element,
  replacements: QueryReplacerMap,
  target: "innerHTML" | "outerHTML" = "outerHTML",
) {
  for (const [selectors, replacer] of Object.entries(replacements)) {
    const nodes = doc.querySelectorAll(selectors);
    for (const node of nodes) {
      node[target] = replacer(node);
    }
  }
}
