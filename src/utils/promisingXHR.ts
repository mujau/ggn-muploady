/* eslint-disable @typescript-eslint/no-explicit-any */
type Request = Tampermonkey.Request<any>;
type Response = Tampermonkey.Response<any>;

const xhr = GM_xmlhttpRequest || GM.xmlHttpRequest;

/**
 * A promise based wrapper for `GM_xmlhttpRequest` similar to native `fetch`.
 * @param details - An object with `details` that should be passed to `GM_xmlhttpRequest`.
 * `onabort`, `onerror`, `ontimeout` and `onload` properties are overriden.
 * @param signal - Abort signal.
 * @returns A promise resolving to `GM_xmlhttpRequest`'s `Response` object.
 */
export function promisingXHR(
  details: Request,
  signal?: AbortSignal,
): Promise<Response> {
  return new Promise((resolve, reject) => {
    Object.assign(details, {
      onabort: (response: Response) => {
        reject(response);
      },
      onerror: (response: Response) => {
        reject(response);
      },
      ontimeout: (response: Response) => {
        reject(response);
      },
      onload: (response: Response) => {
        resolve(response);
      },
    });
    const { abort } = xhr(details);
    if (signal && signal instanceof AbortSignal) {
      signal.addEventListener("abort", () => abort(), { once: true });
    }
  });
}

export default promisingXHR;
