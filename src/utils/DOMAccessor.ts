type ElementTuple = readonly [
  selector: string,
  constructor: typeof HTMLElement,
];

interface Elements {
  [name: string]: ElementTuple;
}

type HasValueConstructor =
  | typeof HTMLInputElement
  | typeof HTMLTextAreaElement
  | typeof HTMLSelectElement;
type HasValue = HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement;

type Accessor<T extends Elements> = {
  -readonly [Property in keyof T as T[Property][1] extends HasValueConstructor
    ? Property
    : never]: string;
} & {
  [Property in keyof T as `${Property & string}Element`]: InstanceType<
    T[Property][1]
  >;
};

const isInputElement = (elem: unknown): elem is HTMLInputElement =>
  elem instanceof HTMLInputElement;
const isTextAreaElement = (elem: unknown): elem is HTMLTextAreaElement =>
  elem instanceof HTMLTextAreaElement;
const isSelectElement = (elem: unknown): elem is HTMLSelectElement =>
  elem instanceof HTMLSelectElement;
const hasValue = (elem: unknown): elem is HasValue =>
  isInputElement(elem) || isTextAreaElement(elem) || isSelectElement(elem);
const getElementValue = (elem: unknown) => (hasValue(elem) ? elem.value : "");
const setElementValue = (elem: unknown, value: unknown) =>
  hasValue(elem) && !elem.disabled ? (elem.value = String(value)) : undefined;

/**
 * Queries DOM with provided selector string and asserts the returned element is of correct type.
 * @param selector - A string with selectors.
 * @param Constructor - A constructor extending HTMLElement.
 * @returns A narrowed-down HTMLElement or undefined.
 */
function getElement<C extends typeof HTMLElement>(
  selector: string,
  Constructor: C,
): InstanceType<C> | undefined {
  const elem = document.querySelector(selector);
  return elem instanceof Constructor ? (elem as InstanceType<C>) : undefined;
}
/**
 * Binds DOM elements to properties for easy access.
 * @privateRemarks Overengineered.
 * @param elements - An object describing DOM elements that should be binded.
 * @returns An object with binded DOM elements.
 */
export function getDOMAccessor<T extends Elements>(elements: T): Accessor<T> {
  const accessor = {} as Accessor<T>;
  for (const [name, [selector, Constructor]] of Object.entries(elements) as [
    keyof Elements,
    ElementTuple,
  ][]) {
    const element = getElement(selector, Constructor);
    if (!element) {
      throw new ReferenceError(`${name} not found`);
    }
    Object.defineProperty(accessor, `${name}Element`, {
      enumerable: true,
      get: () => element,
    });

    if (hasValue(element)) {
      Object.defineProperty(accessor, name, {
        enumerable: true,
        get: () => getElementValue(element),
        set: (value: unknown) => setElementValue(element, value),
      });
    }
  }
  return accessor;
}
