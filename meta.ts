import pkg from "./package.json" assert {type: "json"};

export default `// ==UserScript==
// @name        ${pkg.productName}
// @namespace   green-is-my-paddy
// @version     ${pkg.version}
// @description ${pkg.description}
// @author      ${pkg.author}
// @homepageURL https://gitlab.com/mujau/ggn-muploady
// @downloadURL https://gitlab.com/mujau/ggn-muploady/-/raw/master/dist/ggn-muploady.user.js
// @match       https://gazellegames.net/upload.php
// @require     https://cdn.jsdelivr.net/npm/dompurify@3.1.0/dist/purify.min.js
// @grant       GM_xmlhttpRequest
// @grant       GM.xmlHttpRequest
// @run-at      document-end
// ==/UserScript==\n`;
